%% Init
% 20/08/21 Ben Ripley
% 2D simulation of spinor condensate - S2 method
% Creates inputs parameter file for Spinor_S2_run.m
% Tested: Matlab 2018a, 2019a, 2020a (9.8), 2021a
close all;clear;%clc;
set(0,'defaulttextInterpreter','latex');
dbstop if error;
%% Notes
% set renderer to painters? - vector might take longer to plot, use bitmap atm - is painters quicker than opengl
% ffmpeg:
% cd "C:\Users\ripbe022\Dropbox\BenHonShare\code\Three Component\figs\"
% "C:\Users\ripbe022\Downloads\Program Files\ffmpeg\bin\ffmpeg" -i pspin%d00.jpg -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v h264 pspin.mp4
% "C:\Users\ripbe022\Downloads\Program Files\ffmpeg\bin\ffmpeg" -framerate 60 -i pspin%d000.jpg -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v libx265 -crf 28 pspin.mp4
% "C:\Users\ripbe022\Downloads\Program Files\ffmpeg\bin\ffmpeg" -framerate 60 -i pspin%04d.jpg -vf "crop=670:620:1375:1040" -c:v libx265 -crf 28 -frames:v 1000 pspin.mp4
%
% thunderbirds:
% for i in {21..25} ; do echo $i ; ssh d$i 'nvidia-smi' ; done
% ps -up `nvidia-smi -q -x | grep pid | sed -e 's/<pid>//g' -e 's/<\/pid>//g' -e 's/^[[:space:]]*//'`
% watch -d -n 0.1 nvidia-smi
%% Setup
usegpu = true;
plotstep = 100;             % how often to save plot
psistep = 1e3;              % how often to save psi
% define parameters
maxX = 256;         %128    % grid size (half width)
N_x = 4*maxX;       %512    % num of gridpoints (want at least 2 points per spin healing length, i.e. 4*maxX; must be even for sEk)
q = 3;              %2      % quadratic Zeeman
step_size = 5e-2;   %5e-2   % timestep size
dt = 5e-3;          %5e-3   % S2 step divisions
t_max = 1e4;        %5e3    % maximum spin time


omega = [0 0];      %[0 0]  % trap frequency
c_0 = 3;            %3      % density dependent interactions
c_1 = 1;            %1      % spin dependent interactions
Ly = 0.25;          %0.25   % correlation length fraction of maximum
binsize = 0.1;      %0.1    % width of bins for correlation function data smoothing
sigma_v = [2 1 5];  %[2 1 5]% sigma used in gaussian blur for vortex detection, first entry is plotted (others saved)

maxY = maxX;
N_y = N_x;
n0 = 1;             %1

currdate = datestr(now, 'yyyy-mm-dd_HH-MM-SS'); % set date for file names eg. '2021-05-17_17-44-01'

gi = makestruct(maxX,maxY,N_x,N_y,'fttype',-1);
inputs = makestruct(omega,q,c_0,c_1,t_max,step_size,dt,plotstep,psistep,n0,Ly,binsize,usegpu,currdate,gi,sigma_v);

mkdir('figs', currdate)
paramname = sprintf('figs/%s/params%s', currdate, currdate);
save(paramname, 'inputs')

fprintf('%s\n',currdate)
clear
