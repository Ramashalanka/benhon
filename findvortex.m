function vpos = findvortex(psit,inputs,si)

% Gaussian blur:
fblurk = exp(-inputs.sigma_v(si)^2*inputs.grids.Ksqd2);              % gaussian filter
psik = fftshift(fftn(ifftshift(psit{2})));          % k-space  psi_0
psiv = fftshift(ifftn(ifftshift(fblurk.*psik)));    % smoothed psi_0 (psiblur)

% locate vortices:
% psiv = psit{2};
phase = angle(psiv);
filter1 = [-1 +1;+1 -1];
filter2 = [+1 -1;-1 +1];
vortices = conv2(unwrap(phase,[],1),filter1,'valid') + conv2(unwrap(phase,[],2),filter2,'valid');
position = round(vortices/(2*pi));
[vpx,vpy] = find(position >  0.5); % positive vortices
[vnx,vny] = find(position < -0.5); % negative vortices
vpos = makestruct(vpx,vpy,vnx,vny);

% tic
%% ignore vortex pairs separated by 1 or 2 spin healing lengths
% for-loops are faster on cpu
% vtime = tic;
% vpx = gather(vpx);
% vpy = gather(vpy);
% vnx = gather(vnx);
% vny = gather(vny);
%
% fprintf('nextvs:')
% reverseStr = '';
% % tic
% for vpi = 1:numel(vpy)
%     msg = sprintf('%d/%d (%.1f)',vpi,numel(vpy),100*vpi/numel(vpy));
%     msgp = sprintf('(%.1f)',100*vpi/numel(vpy));
%     fprintf([reverseStr, msg]);
%     reverseStr = repmat(sprintf('\b'), 1, length(msg));
%     reverseStrp = repmat(sprintf('\b'),1, length(msgp));
%     for vni = 1:numel(vny)
%         vdis = sqrt((vpx(vpi)-vnx(vni))^2 + (vpy(vpi)-vny(vni))^2); % calculate distance between vortices (of opposite chirality)
%         if vdis <= 2
%             vpx(vpi) = nan;
%             vpy(vpi) = nan;
%             vnx(vni) = [];
%             vny(vni) = [];
%             break
%         end
%     end
% end
% pindex = isnan(vpx);
% vpx(pindex) = [];
% vpy(pindex) = [];
% fprintf([reverseStrp, sprintf('%.2fs\n',toc(vtime))])
% vpos = makestruct(vpx,vpy,vnx,vny);

%% vectorisation fix - definitely much faster (~7x) but makes very large variables (D = 226GB for maxX = 256, N_x = 4*maX, q=3)
% problem - if several vortices are close together, it will remove all of them, which may break pair symmetry.
% pdist2() retrieved from:
% https://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/29004/versions/2/previews/FPS_in_image/FPS%20in%20image/Help%20Functions/SearchingMatches/pdist2.m/index.html
%
% Let X be an m-by-p matrix representing m points in p-dimensional space
% and Y be an n-by-p matrix representing another set of points in the same
% space. This function computes the m-by-n distance matrix D where D(i,j)
% is the distance between X(i,:) and Y(j,:).  This function has been
% optimized where possible, with most of the distance computations
% requiring few or no loops.
%
% The metric can be one of the following:
%
% 'euclidean' / 'sqeuclidean':
%   Euclidean / SQUARED Euclidean distance.  Note that 'sqeuclidean'
%   is significantly faster.
%
% INPUTS
%  X        - [m x p] matrix of m p-dimensional vectors
%  Y        - [n x p] matrix of n p-dimensional vectors
%  metric   - ['sqeuclidean'], 'chisq', 'cosine', 'emd', 'euclidean', 'L1'
%
% OUTPUTS
%  D        - [m x n] distance matrix

% D = pdist2([vnx vny],[vpx vpy]);
% X = D<=4; % using 4 because using squared euclidian distance (faster) -> 2^2
% X = X & cumsum(X,1)==1 & cumsum(X,2)==1;
% [vni,vpi] = find(X);
% vnx(vni) = [];
% vny(vni) = [];
% vpx(vpi) = [];
% vpy(vpi) = [];
%
% vpos = makestruct(vpx,vpy,vnx,vny);
% % fprintf('%.1fs\n',toc)
%
%
% function D = pdist2( X, Y, metric )
% if( nargin<3 || isempty(metric) ); metric=0; end;
%
% switch metric
%     case {0,'sqeuclidean'}
%         D = distEucSq( X, Y );
%     case 'euclidean'
%         D = sqrt(distEucSq( X, Y ));
%     otherwise
%         error(['pdist2 - unknown metric: ' metric]);
% end
%
% function D = distEucSq( X, Y )
%
% m = size(X,1); n = size(Y,1);
% Yt = Y';
% XX = sum(X.*X,2);
% YY = sum(Yt.*Yt,1);
% D = XX(:,ones(1,n)) + YY(ones(1,m),:) - 2*X*Yt;

%%%% function D = distEucSq( X, Y )
%%%%%%% code from Charles Elkan with variables renamed
%%%% m = size(X,1); n = size(Y,1);
%%%% D = sum(X.^2, 2) * ones(1,n) + ones(m,1) * sum(Y.^2, 2)' - 2.*X*Y';

