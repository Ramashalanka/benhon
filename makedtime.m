function dtime = makedtime(dtime,data,idx)
dtime.allavn(:,idx) = data.avn;
dtime.Fz2t(idx) = data.Fz2;
dtime.Fperp2t(idx) = data.Fperp2;
dtime.align2t(idx) = data.align2;
dtime.alignperp2t(idx) = data.alignperp2;
dtime.alignperpunc2t(idx) = data.alignperpunc2;
dtime.Lt(:,idx) = data.Lpos;
for s = 1:numel(data.vnum(:,1))
    dtime.vnumt{s}(:,idx) = data.vnum(s,:);
end
dtime.Etotalt(idx) = data.Etotal;
dtime.Lvt(:,idx) = data.Lv;