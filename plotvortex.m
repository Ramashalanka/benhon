clear
close all

% load('/Volumes/jdc2/dbaillie/matlabdata/BenSpinor/code/figs/2021-08-06_10-22-26/data/psi7150.mat')
% load('/Volumes/jdc2/dbaillie/matlabdata/BenSpinor/code/figs/2021-08-06_10-22-26/data/inputs.mat')

load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/inputs.mat')
% load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/psi0100.mat') %6830
% load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/psi0300.mat') %6830
load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/psi1000.mat') %6830

close all
psiv = psit{2};
x = inputs.grids.x;
y = inputs.grids.y;

clf
% psiv = inputs.grids.X+1i*inputs.grids.Y; % test vortex at 0,0
phase = angle(psiv);
imagesc(inputs.grids.x,inputs.grids.y,phase.')
set(gca,'ydir','normal','clim',[-pi,pi])
title('$\arg\psi_0$','FontSize',18)
xlabel('$x$','FontSize',18)
ylabel('$y$','FontSize',18)
% xlim(min(grids.x)*[1 -1])
% ylim(min(grids.y)*[1 -1])
axis(gca,'image','tight')
colormap(gca,'hsv')
cb = colorbar;
set(cb,'Ticks',linspace(-pi,pi,5),'TickLabelInterpreter','latex','FontSize',18)
% cb.Ticks = linspace(-pi,pi,5);
% cb.TickLabelInterpreter = 'latex';
cb.TickLabels = {'$-\pi$','$-\frac{\pi}{2}$','$0$','$\frac{\pi}{2}$','$\pi$'};


hold on
filter1 = [-1 1;+1 -1];
filter2 = [1 -1;-1  1];
vortices = conv2(unwrap(phase,[],1),filter1,'valid') + conv2(unwrap(phase,[],2),filter2,'valid');
position = round(vortices/(2*pi));
[vpx,vpy] = find(position >  0.5);
[vnx,vny] = find(position < -0.5);
scatter(x(vpx),y(vpy),50,'k+','linewidth',1)
scatter(x(vnx),y(vny),50,'ko','linewidth',1)

% print('-dsvg',sprintf('figs/2021-08-06_10-22-26/phase1000v'))