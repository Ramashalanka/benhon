function plotresults(currdate)
%% plotresults Creates plots from saved data once run is finished

currdate = '2021-08-05_22-17-30';
datastep = 1000;
corrlennum = 6;
numplotdata = length(dir(['figs/' currdate '/data/plotdata*.mat']));
% load data
load(['figs/' currdate '/data/inputs.mat'])
load(['figs/' currdate '/data/timedata.mat']) % 25 MB file - optimise save and load?
for step = 1:numfiles
    if ~mod(step,datastep)
        S(step/datastep) = load(sprintf('figs/%s/data/curdata%04d.mat',currdate,step));
    end
end


% isn't it fine to just plot nan values? (wont connect lines over gaps)
tOK = ~isnan(dtime.allavn(1,:)); % non-nan indices
tv = inputs.t(tOK); % time vector

% f1 = get(gcf,'number');
h2 = figure(2);
clf(h2)
set(gcf,'position', [-1279          43        1280         907])
Lstyle = 'kr';

subplot(2,6,[1 2])
hold on
for nnum = 1:length(S) % plot of correlation function for each datastep
    sp1(nnum) = plot(S(nnum).dataall.plotdata.rmean,S(nnum).dataall.plotdata.yRemean,'x','MarkerSize',2,'DisplayName',sprintf('$t=%d t_s$',nnum*datastep*inputs.plotstep*inputs.dt)); % ti*dt
    % circles for width measurement position
    Lvalues = [0 inputs.Ly*S(nnum).dataall.plotdata.yRemean(1)];
    Lstyle = 'kr';
    for Ltype = 1:2
        plot(S(nnum).dataall.data.Lpos(Ltype),Lvalues(Ltype),[Lstyle(Ltype) 'o'],'HandleVisibility','off');
    end
end
xlabel('$r$','FontSize',18)
title('$n^{(1)}_0(r)/N_0$','FontSize',18)
legend show
set(legend,'interpreter','latex','box','off')
xlim([0 inputs.gi.maxX])
ylim([-0.5 1])


Lvalues = [0 0.25]; % temporary - should set this as Ly in inputs
for Ltype = 1:2
    subplot(2,6,[(2*Ltype)+1 (2*Ltype)+2])
    hold on
    for nnum = 1:length(S) % plot of correlation function for each datastep
        plot(S(nnum).dataall.plotdata.rmean/dtime.Lt(Ltype,nnum*datastep*inputs.plotstep),S(nnum).dataall.plotdata.yRemean,'x','MarkerSize',2,'DisplayName',sprintf('$t=%d t_s$',nnum*datastep*inputs.plotstep*inputs.dt)) % use ti from S instead
        xlim([0 max(S(nnum).dataall.plotdata.rmean/dtime.Lt(Ltype,nnum*datastep*inputs.plotstep))])
    end
    xlabel(sprintf('$r/L^{%s}(t)$',num2str(Lvalues(Ltype))),'FontSize',18)
    title('$n^{(1)}_0(r)/N_0$','FontSize',18)
    legend show
    set(legend,'interpreter','latex','box','off')
%     xlim([0 inputs.gi.maxX])
    ylim([-0.5 1])
end

% subplot(2,6,[3 4])
% for nnum = 1:length(S) % plot of correlation function for each datastep
%     plot(S(nnum).dataall.plotdata.rmean,S(nnum).dataall.plotdata.yRemean,'x','MarkerSize',2)
% end
% xlabel('$r$','FontSize',18)
% % ylabel('')
% title('$n^{(1)}_0(r)/N_0$','FontSize',18)
% xlim([0 inputs.gi.maxX])
% ylim([-0.5 1])
% 
% subplot(2,6,[5 6])
% for nnum = 1:length(S) % plot of correlation function for each datastep
%     plot(S(nnum).dataall.plotdata.rmean,S(nnum).dataall.plotdata.yRemean,'x','MarkerSize',2)
% end
% xlabel('$r$','FontSize',18)
% % ylabel('')
% title('$n^{(1)}_0(r)/N_0$','FontSize',18)
% xlim([0 inputs.gi.maxX])
% ylim([-0.5 1])

subplot(2,6,[8 9])
hold on
for Ltype = 1:2
    Lthis = dtime.Lt(Ltype,:);
    plot(inputs.t,Lthis,[Lstyle(Ltype) '-']);
end
xlim([0 max(tv)])
xlabel('$t_s$','FontSize',18)
title('$L$','FontSize',18)

subplot(2,6,[10 11])
hold on
for mnum = 1:3
    Lthis = dtime.allavn(mnum,:);
    plot(tv,Lthis)
%     plot(inputs.t,avnthis)
end
xlim([0 max(tv)])
l=legend('$N_+$','$N_0$','$N_-$');
set(l,'interpreter','latex','box','off')
xlabel('$t_s$','FontSize',18)
title('$N$','FontSize',18)

axes('Position',[0 0 1 1],'Visible','off');
text(0.1,0.98,sprintf('Runtime: %s, $t_{max}=%d t_s$',duration(0,0,timetaken),inputs.t_max),'FontSize',18)
text(0.1,0.02,sprintf('$n_0=%.1f$, $(X,Y)=(%0.f,%0.f)$, $(N_x,N_y)=(%0d,%0d)$, $q=%.2f$, $c_0=%.2f$, $c_1=%.2f$, $10^4\\Delta t=%.2f$', inputs.n0, inputs.grids.maxX, inputs.grids.maxY, inputs.grids.N_x, inputs.grids.N_y, inputs.q, inputs.c_0, inputs.c_1, 10^4*inputs.dt),'FontSize',18)
% print('-djpeg',sprintf('figs/%s/data/results', currdate))
return
%% L Comparison (0.25)
h3 = figure(3);
clf(h3)
set(gcf,'position', [1          41        1920         963])
hold on
Sinputs(1) = load('figs/2021-08-05_16-59-34/data/inputs.mat');
Sinputs(2) = load('figs/2021-08-05_22-17-30/data/inputs.mat');
Stime(1) = load(['figs/2021-08-05_16-59-34/data/timedata.mat']);
Stime(2) = load(['figs/2021-08-05_22-17-30/data/timedata.mat']);

plot(Sinputs(1).inputs.t,Stime(1).dtime.Lt(2,:),'DisplayName','$(X,Y)=(32,32)$, $(N_x,N_y)=(128,128)$')
plot(Sinputs(2).inputs.t,Stime(2).dtime.Lt(2,:),'DisplayName','$(X,Y)=(64,64)$, $(N_x,N_y)=(256,256)$')
ax1 = gca;
legend show
set(legend,'interpreter','latex','box','off','FontSize',18,'Location','southeast')
xlabel('$t_s$','FontSize',18)
title('$L$','FontSize',18)
axes('Position',[0 0 1 1],'Visible','off');
text(0.1,0.02,'Files: 2021-08-05\_16-59-34 \& 2021-08-05\_22-17-30','FontSize',12)
% print('-djpeg','figs/2021-08-05_16-59-34/Lcomp')
% axes(ax1)
tuse = ~isnan(Stime(2).dtime.Lt(2,:));
tv = Sinputs(2).inputs.t;
xlim(h3, [0 max(tv(tuse))])

% print('-djpeg','figs/2021-08-05_16-59-34/Lcompzoom')
%% N/N_0 over t
clear all
load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/timedata.mat')
load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/inputs.mat')
% load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/psi1000.mat')

tOK = ~isnan(dtime.allavn(1,:)); % non-nan indices
tv = inputs.t(tOK); % time vector
t_max = 100; % xlim to tmax = ?? t_s
ti_max = t_max/inputs.dt+1;

h4 = figure(4);
clf(h4)
% set(gcf,'position', [1          41        1920         963])
lncols = {'-','-','--'};
hold on
for mnum = 1:3
    avnthis = dtime.allavn(mnum,:);
    avnuse = ~isnan(avnthis);
    plot(tv(avnuse(1:ti_max)),avnthis(avnuse(1:ti_max)),lncols{mnum},'LineWidth',2)
end
% xlim([0 tv(ti+1)]) % inputs.t_max])
l=legend('$N_+$','$N_0$','$N_-$');
set(l,'interpreter','latex','box','off','location','east','FontSize',18)
xlabel('$t_s$','FontSize',18)
title('$N_m/N$','FontSize',18)

% print('-dsvg',sprintf('figs/2021-08-06_10-22-26/Nm'))
%% correlation function
clear all
set(0,'defaulttextInterpreter','latex');

% shotnum = 1;runid = '2021-08-27_19-34-52'; % shot 1
% shotnum = 2;runid = '2021-08-27_19-39-11'; % shot 2
% shotnum = 3;runid = '2021-08-27_19-44-18'; % shot 3
% shotnum = 4;runid = '2021-08-27_19-46-17'; % shot 4
shotnum = 5;runid = '2021-08-27_19-46-34'; % shot 5

texdate = strrep(runid,'_','\_');
runnum = 4;

load(['C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/code/figs/' runid '/data/timedata.mat'])
load(['C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/code/figs/' runid '/data/inputs.mat'])

h5 = figure(5);
clf(h5),ax = axes();hold on
% set(gcf,'position', [782   558   458   420])
set(gcf,'position', [1          41        1920         963])
t_max = inputs.t_max; % xlim to tmax = ?? t_s
ti_max = t_max/inputs.step_size+1;

for step = 1:10
    pdata(step) = load(sprintf('C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/code/figs/%s/data/plotdata%04d.mat',runid,step*100));
    Lpos(step) = dtime.Lt(2,pdata(step).ti);
    h(step) = plot(pdata(step).plotdata.rmean./Lpos(step),pdata(step).plotdata.yRemean,'x','MarkerSize',2,'DisplayName',sprintf('$t=%d$ $t_s$',pdata(step).ti*inputs.step_size));
end
plot(1,0.25,'ro','DisplayName','$L$')

% legend customisation
hCopy = copyobj(h, ax); 
set(hCopy,'XData', NaN', 'YData', NaN)
% arrayfun(@(h)set(h,'XData',nan(size(h.XData))),hCopy)
for ii=1:numel(hCopy)
    hCopy(ii).MarkerSize = 15;
end
legend(hCopy)
legend('interpreter','latex','box','off','FontSize',18)

xlabel('$r/L$','FontSize',18)
% ylabel('')
title('$n^{(1)}_0(r)/N_0$','FontSize',18)
% xlim([0 inputs.gi.maxX])
xlim([0 14])
ylim([-0.3 1])

axes('Position',[0 0 1 1],'Visible','off');
text(0.1,0.02,sprintf('%s: $n_0=%.1f$, $(X,Y)=(%0.f,%0.f)$, $(N_x,N_y)=(%0d,%0d)$, $q=%.2f$, $c_0=%.2f$, $c_1=%.2f$, $t_\\mathrm{step}=%.2g$, $\\Delta t=%.2g$', ...
    texdate, inputs.n0, inputs.grids.maxX, inputs.grids.maxY, inputs.grids.N_x, inputs.grids.N_y, inputs.q, inputs.c_0, inputs.c_1, inputs.step_size, inputs.dt),'FontSize',18)

print('-djpeg',sprintf('figs/plots/run%d/collapse/collapse-s%d',runnum,shotnum))
%% L
clear all

load('C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/timedata.mat')
load('C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/inputs.mat')
% load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/psi1000.mat')

tOK = ~isnan(dtime.Lt(2,:)); % non-nan indices
tv = inputs.t(tOK); % time vector
t_max = 100; % xlim to tmax = ?? t_s
ti_max = t_max/inputs.dt+1;

h6 = figure(6);
clf(h6)
% set(gcf,'position', [782   558   458   420])

Lthis = dtime.Lt(2,:);
Luse = ~isnan(Lthis);
plot(tv(Luse),Lthis(Luse),'LineWidth',2)
% xlim([0 tv(length(tv))]) % out to max time
% xlim([0 tmax]) % limit to certain time
xlabel('$t_s$','FontSize',18)
title('$L$','FontSize',18)
% axis square

% print('-dsvg',sprintf('figs/2021-08-06_10-22-26/Lt100_s'))
%% log(L)
clear all
load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/timedata.mat')
load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/inputs.mat')
% load('C:/Users/ripbe022/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/psi1000.mat')

tOK = ~isnan(dtime.Lt(1,:)); % non-nan indices
tv = inputs.t(tOK); % time vector
t_max = 100; % xlim to tmax = ?? t_s
ti_max = t_max/inputs.dt+1;

h7 = figure(7);
clf(h7)
% set(gcf,'position', [1          41        1920         963])
Lthis = dtime.Lt(2,:);
Luse = ~isnan(Lthis);
loglog(tv(Luse),Lthis(Luse),'LineWidth',2)
% xlim([0 tv(length(tv))]) % inputs.t_max])
xlim([0.1 1e8])
ylim([0 1e3])
xlabel('$\log_{10}{t_s}$','FontSize',18)
title('$\log_{10}{L}$','FontSize',18)

% print('-dsvg',sprintf('figs/2021-08-06_10-22-26/logLtend_spaced'))
%% L compare
clear all
foldir = dir('C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs');
fname = {foldir(3:7).name};
h8 = figure(8);
clf(h8)
hold on
for fnum = 1:length(fname)
    St(fnum) = load(sprintf(['C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/' fname{fnum} '/data/timedata.mat']));
    Si(fnum) = load(sprintf(['C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/' fname{fnum} '/data/inputs.mat']));

    tOK = ~isnan(St(fnum).dtime.Lt(2,:)); % non-nan indices
    tv = Si(fnum).inputs.t(tOK); % time vector
%     tmax = 100; % xlim to tmax = ?? t_s
%     tmaxi = tmax/inputs.dt+1;

    % set(gcf,'position', [782   558   458   420])

    Lthis = St(fnum).dtime.Lt(2,:);
    Luse = ~isnan(Lthis);
    plot(tv(Luse),Lthis(Luse),fname{fnum})%,'LineWidth',2)
    % xlim([0 tv(length(tv))]) % out to max time
    % xlim([0 tmax]) % limit to certain time
    xlabel('$t_s$','FontSize',18)
    title('$L$','FontSize',18)
    % axis square
end 
% print('-dsvg',sprintf('figs/2021-08-06_10-22-26/Lt100_s'))

%% Vortices
function vpos = findvortex(psit,inputs)

% load('C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/inputs.mat')
% load('C:/Users/T/OneDrive - University of Otago/BenDannyBlairOD/DannyResults/figs/2021-08-06_10-22-26/data/psi1000.mat') %6830

psiv = psit{2};
x = inputs.grids.x;
y = inputs.grids.y;

% % phase plot
% clf
% % psiv = inputs.grids.X+1i*inputs.grids.Y; % test vortex at 0,0
phase = angle(psiv);
% imagesc(inputs.grids.x,inputs.grids.y,phase.')
% set(gca,'ydir','normal','clim',[-pi,pi])
% title('$\arg\psi_0$','FontSize',18)
% xlabel('$x$','FontSize',18)
% ylabel('$y$','FontSize',18)
% % xlim(min(grids.x)*[1 -1])
% % ylim(min(grids.y)*[1 -1])
% axis(gca,'image','tight')
% colormap(gca,'hsv')
% cb = colorbar;
% set(cb,'Ticks',linspace(-pi,pi,5),'TickLabelInterpreter','latex','FontSize',12)
% cb.TickLabels = {'$-\pi$','$-\frac{\pi}{2}$','$0$','$\frac{\pi}{2}$','$\pi$'};

% hold on
filter1 = [-1 1;+1 -1];
filter2 = [1 -1;-1  1];
vortices = conv2(unwrap(phase,[],1),filter1,'valid') + conv2(unwrap(phase,[],2),filter2,'valid');
position = round(vortices/(2*pi));
[vpx,vpy] = find(position >  0.5); % positive vortices
[vnx,vny] = find(position < -0.5); % negative vortices

vpos = makestruct(vpx,vpy,vnx,vny);
% scatter(x(vpx),y(vpy),50,'k+','linewidth',1) % plot vortices on phase plot
% scatter(x(vnx),y(vny),50,'ko','linewidth',1)

% print('-dsvg',sprintf('figs/2021-08-06_10-22-26/phase1000v'))
%%
% pic = imshow('figs/plots/run8/allLlog.jpg');
A=[1 2 5];
B=[4 3];
C=[4 3 2 1 5];
alldata={A,B,C};%put them all in a cell for convenience
% pad with NaNs here
maxlen = max(cellfun('prodofsize',alldata));
for n=1:numel(alldata)
    current_elem=numel(alldata{n});
    if current_elem<maxlen
        alldata{n}((current_elem+1):maxlen)=NaN;
    end
end
extradim=1+ndims(alldata{1});
bigarray=cat(extradim,alldata{:});
mean(bigarray,extradim,'omitnan')
%% plotall (danny)
close all
clear
figure
hold on
dateAll = {'2021-08-06_10-22-26','2021-08-05_22-17-30','2021-08-06_16-55-28','2021-08-06_16-56-42','2021-08-06_16-58-21'};
lstr = {'base','size','low q','grid density','dt'};
for dnum = 1:numel(dateAll)
    tdataname = sprintf('%s/data/', dateAll{dnum});
    tload = load([tdataname 'timedata.mat']);
    iload = load([tdataname 'inputs.mat']);
    plot(iload.inputs.t, tload.dtime.Lt(2,:),'linewidth',2)
end

legend(lstr)
%% vortex correction
% clear
% load('C:\Users\T\OneDrive - University of Otago\BenDannyBlairOD\code\figs\2021-09-12_17-19-17\r1\data\inputs.mat')
% load('C:\Users\T\OneDrive - University of Otago\BenDannyBlairOD\code\figs\2021-09-12_17-19-17\r1\data\psi1990CPU.mat')
grids = inputs.grids;
phase = angle(psit{2});

% phase plot
figure(3);clf(3);hold on;
imagesc(grids.x,grids.y,phase.')
set(gca,'ydir','normal','clim',[-pi,pi])
axis(gca,'image','tight') % daspect([1 1 1])
colormap(gca,'hsv')
colorbar

% vortices
% vpos = findvortex(psit,inputs); %
scatter(grids.x(vpos.vpx),grids.y(vpos.vpy),50,'w+','linewidth',1)
scatter(grids.x(vpos.vnx),grids.y(vpos.vny),50,'wo','linewidth',1)

% post-processing
vpos2 = findvortex(psit,inputs);
scatter(grids.x(vpos2.vpx),grids.y(vpos2.vpy),50,'k+','linewidth',1)
scatter(grids.x(vpos2.vnx),grids.y(vpos2.vny),50,'ko','linewidth',1)
