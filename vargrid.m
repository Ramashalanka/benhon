function [v,dv1,kv,dkv] = vargrid(maxV,N_v)
%vargrid Constructs discrete position-space and k-space axes for a variable
v = linspace(-maxV,maxV,N_v+1);                                             % Make v grid
v = v(1:end-1);                                                             % Cut off last point for periodic boundary conditions (-v = v)
dv1 = v(2)-v(1);                                                            % Grid spacing size
kv_max = N_v*pi/(2*maxV);
kv = linspace(-kv_max,kv_max,N_v+1);
kv = kv(1:end-1);
dkv = pi/maxV;
end