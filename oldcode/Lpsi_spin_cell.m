function Lall = Lpsi_spin_cell(t,psi,inputs,grids)
%Lpsin Calculates energy (kinetic plus potential)
Lall = cell(3,1);
F_z = abs(psi{1}).^2 - abs(psi{3}).^2;
n_0 = abs(psi{2}).^2;
n = abs(psi{1}).^2 + abs(psi{2}).^2 + abs(psi{3}).^2;
for cnum = 1:3
    m = 2 - cnum; % 1, 0, -1
    phit = fftshift(fftn(ifftshift(psi{cnum})))*grids.dr;
    Lk = grids.Ksqd2.*phit;
    Lthis = fftshift(ifftn(ifftshift(Lk)))/grids.dr;
    if max(abs(imag(Lthis(:)))) < 1e-12 % Clip out negligible imag components
        Lthis = real(Lthis);
    end
    
    Veff = inputs.q*m^2 + grids.Vpot + inputs.c_0*n;%abs(psi{cnum}).^2;
    switch m
        case 1
            I_this = (n_0 + F_z).*psi{1} + psi{2}.^2.*conj(psi{3});
        case 0
            I_this = (n - n_0).*psi{2} + 2*psi{1}.*psi{3}.*conj(psi{2}) ;
        case -1
            I_this = (n_0 - F_z).*psi{3} + psi{2}.^2.*conj(psi{1});
    end
    Lall{cnum} = -1i*inputs.dt*(Lthis + Veff.*psi{cnum} + inputs.c_1*I_this);
end
