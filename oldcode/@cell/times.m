function C = times(A,B) % times for cells
if isequal(size(A), size(B)) % cell arrays are same size
    C = cellfun(@times,A,B,'UniformOutput',false); % apply times cellwise
elseif isscalar(A) || isscalar(B) % one summand is scalar
    if isscalar(A)
        C = cell(size(B));
        for n = 1:length(B)
            C{n} = A.*B{n};
        end
    else
        C = cell(size(B));
        for n = 1:length(A)
            C{n} = A{n}.*B;
        end
    end
else % should also account for multiplication with compatible sizes
    error('Cell factor dimensions must agree.')
end
end