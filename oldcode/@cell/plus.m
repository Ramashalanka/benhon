function C = plus(A,B) % plus for cells
if isequal(size(A), size(B)) % cell arrays are same size
    C = cellfun(@plus,A,B,'UniformOutput',false); % apply plus cellwise
elseif isscalar(A) || isscalar(B) % one summand is scalar
    if isscalar(A)
        C = cell(size(B));
        for n = 1:length(B)
            C{n} = A + B{n};
        end
    else
        C = cell(size(B));
        for n = 1:length(A)
            C{n} = A{n} + B;
        end
    end
else % should also account for addition with compatible sizes
    error('Cell summand dimensions must agree.')
end
end
% Two inputs have compatible sizes if, for every dimension, the dimension
% sizes of the inputs are either the same or one of them is 1.