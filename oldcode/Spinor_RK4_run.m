function Spinor_RK4_run(psi, inputs)


timestart = stime;
currdate = datestr(now, 'yyyy-mm-dd_HH-MM-SS'); % set date for file names eg. '2021-05-17_17-44-01'
mkdir('figs',[currdate '/data'])
mkdir('figs',[currdate '/pspin'])
inputs.currdate = currdate;

gi = inputs.gi;
maxX = gi.maxX;
maxY = gi.maxY;
N_x = gi.N_x;
N_y = gi.N_y;
infostr = sprintf('%s: N=(%5d,%5d), R=(%6.1f,%6.1f), q=%3.1f, dt=%.6f\n', currdate, N_x, N_y, maxX, maxY, inputs.q, inputs.dt);
fprintf(infostr);
fout = fopen('figs/rundetails.txt', 'a');
fprintf(fout, infostr);
fclose(fout);


[x,dx1,kx,dkx] = vargrid(maxX,N_x);
[y,dy1,ky,dky] = vargrid(maxY,N_y);

dr = dx1*dy1;
dkr = dkx*dky;
[X,Y] = ndgrid(x,y);
R = sqrt(X.^2+Y.^2);

[KX,KY] = ndgrid(kx,ky);
Ksqd2 = (KX.^2 + KY.^2)/2;
Vpot = 0.5.*(inputs.omega(1)^2*X.^2+inputs.omega(2)^2*Y.^2);      % harmonic trap

gridsCPU = makestruct(maxX,maxY,N_x,N_y,x,y,X,Y,R,Vpot,Ksqd2,dr,dkr,'fttype',-1);
dt = inputs.dt;
t = (0:dt:inputs.t_max)';
inputs.t = t;
inputs.N0 = 4*maxX*maxY*inputs.n0;
inputs.N0sq = inputs.N0*inputs.n0;
inputs.ninitial = sum(xquadspin(psi,gridsCPU)/inputs.N0); % sum(allavn(:,1))
inputs.grids = gridsCPU;

if inputs.usegpu && gpuDeviceCount > 0
    grids = makestruct(maxX,maxY,N_x,N_y,dr,dkr,'fttype',-1);
    grids.x = gpuArray(x);
    grids.y = gpuArray(y);
    grids.X = gpuArray(X);
    grids.Y = gpuArray(Y);
    grids.R = gpuArray(R);
    grids.Vpot = gpuArray(Vpot);
    grids.Ksqd2 = gpuArray(Ksqd2);
    inputs.gpuUsed = true;
    psiCPU = psi;
    psi = cell(3,1);
    for cnum = 1:3
        psi{cnum} = gpuArray(psiCPU{cnum});
    end
    fprintf('using GPU\n')
else
    grids = gridsCPU;
    inputs.gpuUsed = false;
    fprintf('no GPU\n')
end
grids.r = R(:);
save(sprintf('figs/%s/data/inputs', inputs.currdate), 'inputs')
inputs.grids = grids; % overwrite gridsCPU if GPU
psit = psi;

% preallocate
dtime = struct('allavn', nan(3,length(t)),'Fz2t', nan(1,length(t)), 'Fperp2t', nan(1,length(t)), 'align2t', nan(1,length(t)), ...
    'alignperp2t', nan(1,length(t)), 'alignperpunc2t', nan(1,length(t)), 'Lt', nan(2,length(t)), 'vnumt', nan(2,length(t)));
dataall = calcdata(psit,inputs);

% initial values
data = dataall.data;
dtime.allavn(:,1) = data.avn;
dtime.Fz2t(1) = data.Fz2;
dtime.Fperp2t(1) = data.Fperp2;
dtime.align2t(1) = data.align2;
dtime.alignperp2t(1) = data.alignperp2;
dtime.alignperpunc2t(1) = data.alignperpunc2;
dtime.Lt(:,1) = data.Lpos;
dtime.vnumt(:,1) = data.vnum;

% initial plot (plotspinangle uses h1, plotgrowth uses h2)
plotspinangle(psit,t,dataall,dtime,inputs,0);

% fnum = figure(1); % define global fig to stop new plot stealing focus - wont work if making more than 1 plot
% maybe write function to construct figure(s) and axes - then plot to specific axes
fprintf('%d:', length(t)-1)
reverseStr = '';
for ti=1:(length(t)-1)                              % RK4
    msg = sprintf('%d',ti);
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
    
    k1 = Lpsi_spin_cell(t(ti),psit,inputs,grids);
    k2 = Lpsi_spin_cell(t(ti)+dt/2,psit+(1/2.*k1),inputs,grids);
    k3 = Lpsi_spin_cell(t(ti)+dt/2,psit+(1/2.*k2),inputs,grids);
    k4 = Lpsi_spin_cell(t(ti)+dt,psit+k3,inputs,grids);
    for cnum = 1:3
        psit{cnum} = psit{cnum}+(1/6)*(k1{cnum}+2*k2{cnum}+2*k3{cnum}+k4{cnum});
    end
    dataall = calcdata(psit,inputs);
    data = dataall.data;
    dtime.allavn(:,ti+1) = data.avn;
    dtime.Fz2t(ti+1) = data.Fz2;
    dtime.Fperp2t(ti+1) = data.Fperp2;
    dtime.align2t(ti+1) = data.align2;
    dtime.alignperp2t(ti+1) = data.alignperp2;
    dtime.alignperpunc2t(ti+1) = data.alignperpunc2;
    dtime.Lt(:,ti+1) = data.Lpos;
    dtime.vnumt(:,ti+1) = data.vnum;
    
    
    if ~mod(ti,inputs.plotstep) % save every (plotstep)th timestep to file
        if any(isnan(reshape(psi{1},[],1)))
            return % kill if sim blows up (could replace "if..." with assert)
        end
        timetaken = stime - timestart;
        save(sprintf('figs/%s/data/timedata', inputs.currdate), 'dtime', 'timetaken')
        plotspinangle(psit,t,dataall,dtime,inputs,ti); % create figures/plots
        plotdata = dataall.plotdata;
        save(sprintf('figs/%s/data/plotdata%04d', inputs.currdate, ti/inputs.plotstep), 'plotdata', 'ti')
    end
    if ~mod(ti,inputs.psistep) % save psi every (psistep)th timestep to file
        save(sprintf('figs/%s/data/psi%04d', inputs.currdate, ti/inputs.plotstep), 'psit', 'ti')
    end
end
fprintf('\n')
%{
tuse = ~isnan(Fz2t);
% total density plot
f2 = figure(2);
for cnum = 1:3
    subplot(1,3,cnum)
    plot(t(tuse),allavn(cnum,tuse))
    title(sprintf('$N_{%d}/N$',2-cnum),'FontSize',16)
    xlabel('$t/t_s$','FontSize',14)
end
print(f2,'-dsvg',sprintf('figs/%s/density',inputs.currdate))

f4 = figure(4);
plot(t(tuse),Fz2t(tuse))
xlabel('$t/t_s$','FontSize',14)
ylabel('$\langle F_\nu^2\rangle /n^2$','FontSize',14)
hold on
plot(t(tuse),Fperp2t(tuse))
hold off
l4=legend('$\langle F_z^2\rangle$','$\langle F_\perp^2\rangle$');
set(l4,'Interpreter','latex','FontSize',18,'location','northeast');
xlim([0 35])
print(f4,'-dsvg',sprintf('figs/%s/F2',inputs.currdate))


% Fz^2 and Fperp plot
f3 = figure(3);
plot(t(tuse),log10(Fz2t(tuse)))
xlabel('$t/t_s$','FontSize',14)
ylabel('$\log_{10}(\langle F_\nu^2\rangle /n^2$)','FontSize',18)
hold on
plot(t(tuse),log10(Fperp2t(tuse)))
hold off
l3=legend('$\log_{10}\langle F_z^2\rangle$','$\log_{10}\langle F_\perp^2\rangle$');
set(l3,'Interpreter','latex','FontSize',18,'location','northeast');
print(f3,'-dsvg',sprintf('figs/%s/logF2',inputs.currdate))

% plot(t(tuse),align2t(tuse))
% plotgrowth(inputs.currdate)
% plotresults(inputs.currdate)
%}