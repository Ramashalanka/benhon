%% Init
% 14/05/21 Ben Ripley
% 2D simulation of spinor condensate
% Tested: Matlab 2019a, 2020a (9.8)
close all;clear;%clc;
set(0,'defaulttextInterpreter','latex');
dbstop if error;
usegpu = false;
plotstep = 1000 % how often to save plot
psistep = 1e4 % how often to save psi
%% Notes
% Before running:
%   check plotspin for plot position
%   check timestep and max time
% Experiment with parameters: 
%   grid size (same spacing, larger size)
%   q
%   c_1 (try to match Williamson's results for c_1 < 0)

% set renderer to painters? - vector might take longer to plot, use bitmap atm - is painters quicker than opengl
% ffmpeg:
% cd "C:\Users\ripbe022\Dropbox\BenHonShare\code\Three Component\figs\"
% "C:\Users\ripbe022\Downloads\Program Files\ffmpeg\bin\ffmpeg" -i pspin%d00.jpg -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v h264 pspin.mp4
% "C:\Users\ripbe022\Downloads\Program Files\ffmpeg\bin\ffmpeg" -framerate 60 -i pspin%d000.jpg -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -c:v libx265 -crf 28 pspin.mp4
% "C:\Users\ripbe022\Downloads\Program Files\ffmpeg\bin\ffmpeg" -framerate 60 -i pspin%04d.jpg -vf "crop=670:620:1375:1040" -c:v libx265 -crf 28 -frames:v 1000 pspin.mp4
%% Setup
% define parameters
maxX = 32; %32          % grid size (half width)
N_x = 128; %128         % num of gridpoints (want at least 2 points per spin healing length)
q = 2;     %2           % quadratic Zeeman
dt = 1e-4; %1e-4        % timestep size


omega = [0 0];
c_0 = 3;   %3           % density and spin dependent interactions
c_1 = 1;   %1
t_max = 1e3; %1e3
Ly = 0.25; %0.25        % correlation length fraction of maximum
binsize = 0.1; %0.1     % width of bins for correlation function data smoothing

maxY = maxX;
N_y = N_x;
n0 = 1; %1

% Define wavefunction parameters
rng('shuffle')
psi = cell(3,1); % 1, 0, -1
for cnum = 1:3
    psi{cnum} = (randn(N_x,N_y) + 1i*randn(N_x,N_y))*sqrt(n0)/25; % random noise
end
%psi{2} = psi{2} + sqrt(n0)*ones(N_x,N_y); % polar
for cnum = [1 3] % AF
    psi{cnum} = psi{cnum} + sqrt(n0/2)*ones(N_x,N_y);
end

inputs = makestruct(omega,q,c_0,c_1,t_max,dt,plotstep,psistep,n0,Ly,binsize,usegpu);
inputs.gi = makestruct(maxX,maxY,N_x,N_y,'fttype',-1);

Spinor_RK4_run(psi, inputs)