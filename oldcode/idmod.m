% ignore this, just ideas that don't work for file naming
function inputs = idmod(inputs)
%
currdate = datestr(now, 'yyyy-mm-dd_HH-MM-SS'); % set date for file names eg. '2021-05-17_17-44-01'
runnum = 7;
shotnum = 1;
shotmax = 5;
testnum = 1;

% runtitle = sprintf('t%06d',testnum);
runtitle = sprintf('r%03ds%02d',runnum,shotnum);

runids = makestruct(currdate,runnum,shotnum,testnum);
save('runid', 'runids')


%% fscan
% ideas for changing file name / timestamp:
% pull prev index from file?
% normal run:   r001s01
% test tun:     t000001
%
% test = false;
% if test
%     testnum = 7;
%     runtitle = sprintf('t%06d',testnum);
% else
%     runnum = 3;
%     shotnum = 1;
%     runtitle = sprintf('r%03ds%02d',runnum,shotnum);
% end


% fgetl()

infostr = sprintf('%s:(X,Y)=(%5d,%5d)\n',runtitle,inputs.maxX,inputs.maxY);

clear all
filename = 'runids.txt';
fileID = fopen(filename);
data = textscan(fileID,'%s%d%s');
runids = cell2struct(num2cell(data{2}),data{1},1);
fseek(fileID,0,'bof');
% writetable(struct2table(runids),'')

% save('myFile2.mat', 'runnum', 'shotmax', 'shotnum','testnum', '-ASCII');

% infostr = sprintf('run     7 2021-09-05_00-41-03\nshot    1 2021-09-05_00-41-03\ntest    1 2021-09-05_00-41-03\nshotmax 5',);
%% table version
clear
currdate = datestr(now, 'yyyy-mm-dd_HH-MM-SS'); % eg. '2021-05-17_17-44-01'
testrun = false;
filename = 'runids.txt';
runids = table2struct(readtable(filename,'Delimiter',',')); % read data into struct
runid = runids(length(runids)); % extract most recent entry
% runidx = length(runids);

% create new runid entry
runid.currdate = currdate;
if testrun
    % increment testnum
    runid.testnum = runid.testnum+1;
    runtitle = sprintf('t%06d',runid.testnum);
else
    if runid.shotnum < runid.shotmax
        % increment shotnum
        runid.shotnum = runid.shotnum+1;
    else
        % increment runnum, reset shotnum
        runid.runnum = runid.runnum+1;
        runid.shotnum = 1;
    end
    runtitle = sprintf('r%03ds%03d',runid.runnum,runid.shotnum);
end

runstring = sprintf('\n%s,%d,%d,%d,%d',currdate,runid.runnum,runid.shotnum,runid.shotmax,runid.testnum);
% check directory doesnt already exist
assert(~isfolder(['figs/' runtitle]),sprintf('Directory with run-name %s in directory "figs" already exits - check runids.txt file is up to date.', runstring))

% append new string on new line with currdate
fid = fopen(filename,'a');
fprintf(fid, runstring);
fclose(fid);


% mkdirs
% mkdir('figs',[runtitle '/data'])
% mkdir('figs',[runtitle '/pspin'])
%%
[ret, name] = system('hostname');
if ret ~= 0
    if ispc
        name = getenv('COMPUTERNAME');
    else
        name = getenv('HOSTNAME');
    end
end
name = strtrim(lower(name));

%%
clear
close all
x = logspace(-1,2,100);
y = @(x) x.^5;
loglog(x,y(x),'bs-','DisplayName','log 1')
y2 = @(x) x.^2;
hold on
loglog(x,y2(x),'r','DisplayName','log 2')
legend show
figure()
x = linspace(0.1,100,50);
plot(x,y(x),'bs-')
hold on
plot(x,y2(x),'r')
% legend('y1','y2','location','northwest')

%% notes
% x = logspace()

%% extras

% 2021-09-05_00-41-03
% r007
% s001
% t001

% run,shot,test,shotmax,currdate
% 7,1,1,5,2021-09-05_17-23-52

% runnum      7   2021-09-05_00-41-03
% shotnum     1   2021-09-05_00-41-03
% testnum     1   2021-09-05_00-41-03
% shotmax     5

% currdate,runnum,shotnum,shotmax,testnum
% 2021-09-05_17-23-52,7,1,5,1
% 2021-09-05_17-24-21,7,2,5,1
