%% AF Energies
c_0 = 3;
c_1 = 1;
n = 1;
ek = 0:0.01:100; %x: epsilon_k
q = -100:0.1:100; %y: quadratic zeeman
qtest = 2;
[EK,Q] = ndgrid(ek,q);
E2_plus = @(ek) ek.*(ek+(2*c_0*n));
E2_minus = @(ek,q) ek.*(ek+(2*c_1*n));
E2_zero = @(ek,q) (ek-q).*(ek-q+(2*c_1*n));

close all
figure(1)
set(gcf,'position', [1          41        1920         963])
subplot(1,3,1)
plot(ek,E2_plus(ek))
title('$E_{\mathbf{k},+}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')

subplot(1,3,2)
y = E2_zero(ek,qtest);
plot(ek,y)
hold on
plot(ek,zeros(size(ek)))
hold off
title('$E_{\mathbf{k},0}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')

subplot(1,3,3)
plot(ek,E2_minus(ek))
title('$E_{\mathbf{k},-}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
print('-djpeg',sprintf('figs/temp/fig1'))

figure(2)
set(gcf,'position', [-1279          43        1280         907])
subplot(2,3,1)
imagesc(ek,q,E2_plus(EK))
title('$E_{\mathbf{k},+}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
ylabel('$q$')
set(gca,'ydir','normal')
% xlim(minx*[1 -1])
% ylim(miny*[1 -1])
axis(gca,'tight')
colormap(gca,'parula')
colorbar

subplot(2,3,2)
imagesc(ek,q,E2_zero(EK,Q))
title('$E_{\mathbf{k},0}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
ylabel('$q$')
set(gca,'ydir','normal')
axis(gca,'tight')
colormap(gca,'parula')
colorbar

subplot(2,3,3)
imagesc(ek,q,E2_minus(EK))
title('$E_{\mathbf{k},-}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
ylabel('$q$')
set(gca,'ydir','normal')
axis(gca,'tight')
colormap(gca,'parula')
colorbar

subplot(2,3,4)
imagesc(ek,q,gt(E2_plus(EK),0))
title('$E_{\mathbf{k},+}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
ylabel('$q$')
set(gca,'ydir','normal')
% xlim(minx*[1 -1])
% ylim(miny*[1 -1])
axis(gca,'tight')
colormap(gca,'parula')
colorbar

subplot(2,3,5)
imagesc(ek,q,gt(E2_zero(EK,Q),0))
title('$E_{\mathbf{k},0}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
ylabel('$q$')
set(gca,'ydir','normal')
axis(gca,'tight')
colormap(gca,'parula')
colorbar

subplot(2,3,6)
imagesc(ek,q,gt(E2_minus(EK),0))
title('$E_{\mathbf{k},-}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
ylabel('$q$')
set(gca,'ydir','normal')
axis(gca,'tight')
colormap(gca,'parula')
colorbar

axes('Position',[0 0 1 1],'Visible','off');
text(0.1,0.02,sprintf('$n=%.1f$, $c_0=%.2f$, $c_1=%.2f$', n, c_0, c_1),'FontSize',20)
print('-djpeg',sprintf('figs/temp/fig2'))

figure(3)
set(gcf,'position', [1          41        1920         963])
plot(ek,y)
hold on
plot(ek,zeros(size(ek)))
hold off
title('$E_{\mathbf{k},0}^2$','FontSize',18)
xlabel('$\epsilon_{\mathbf{k}}$')
xlim([17 21])
ylim([-4 4])
% print('-djpeg',sprintf('figs/temp/fig3'))
