function plotrunold(runnum)
% Legacy plotrun.m - for number-indexed runs (different folder timestamps).
% Run must be complete.

% to-do:
% fix plotting incomplete runs
% plot parameters
% loop to find figures when saving

close all
set(0,'defaulttextInterpreter','latex');

if nargin == 0
    runnum = input('Enter run number (between 3 and 7)\n');
end

switch runnum
    case 3
        dateAll = {'2021-08-06_10-22-26','2021-08-05_22-17-30','2021-08-06_16-55-28','2021-08-06_16-56-42','2021-08-06_16-58-21'}; % run3?
    case 4
        dateAll = {'2021-08-27_19-34-52','2021-08-27_19-39-11','2021-08-27_19-44-18','2021-08-27_19-46-17','2021-08-27_19-46-34'}; % run4
    case 5
        dateAll = {'2021-08-30_02-20-21','2021-08-30_02-20-47','2021-08-30_02-21-06','2021-08-30_02-21-19','2021-08-30_02-21-30'}; % run5
    case 6
        dateAll = {'2021-09-02_11-38-15','2021-09-02_11-39-43','2021-09-02_11-40-00','2021-09-02_11-42-00','2021-09-02_11-42-13'}; % run6
    case 7
        dateAll = {'2021-09-05_01-24-11','2021-09-05_01-25-43','2021-09-05_01-25-57','2021-09-05_01-26-19','2021-09-05_01-26-31'}; % run7
    otherwise
        fprintf('Not a valid run number.\n')
        return
end

dateAllstr = strrep(dateAll,'_','\_');

runname = sprintf(['run' num2str(runnum)]); % sets folder for plots e.g. 'run5'
[~,~] = mkdir('figs',['plots/' runname]); % makes directory in e.g. 'figs/plots/run5'

figname = {};
% nmax = 0;
for dnum = 1:numel(dateAll) % load data
    datapath = sprintf('figs/%s/data/', dateAll{dnum});
    St(dnum) = load([datapath 'timedata_old.mat']);
    Si(dnum) = load([datapath 'inputs.mat']);
    t_max(dnum) = Si(dnum).inputs.t_max; % fix this
    % end
    % nmax = max(t_max);
    % for dnum = 1:numel(dateAll) % plot
    %     t_max = Si(dnum).inputs.t_max;
    %     nmax = max(numel(nmax),numel(t_max));
    Lt(dnum,:) = St(dnum).dtime.Lt(2,:);
    Lvt(dnum,:) = St(dnum).dtime.Lvt(:);
    iuse = ones(1,length(Lt));
    t = Si(dnum).inputs.t(iuse);
    
    %     figure(1),hold on,figname{1} = 'L';                     % L
    %     plot(Si(dnum).inputs.t, Lt(dnum,:),'DisplayName',dateAllstr{dnum})%,'linewidth',2)
    %     title('$L$','FontSize',18)
    %     xlabel('$t_s$','FontSize',18)
    %
    %     figure(2),hold on,figname{2} = 'Llog';                  % log L
    %     plot(log10(Si(dnum).inputs.t), log10(Lt(dnum,:)),'DisplayName',dateAllstr{dnum})
    %     title('$\log{L}$','FontSize',18)
    %     xlabel('$\log{t_s}$','FontSize',18)
    %
    %     figure(3),hold on,figname{3} = 'Lv';                    % Lv
    %     plot(Si(dnum).inputs.t, Lvt(dnum,:),'DisplayName',dateAllstr{dnum})
    %     title('$L_v$','FontSize',18)
    %     xlabel('$t_s$','FontSize',18)
    %
    %     figure(4),hold on,figname{4} = 'Lvlog';                 % log Lv
    %     plot(log10(Si(dnum).inputs.t), log10(Lvt(dnum,:)),'DisplayName',dateAllstr{dnum})
    %     title('$\log{L_v}$','FontSize',18)
    %     xlabel('$\log{t_s}$','FontSize',18)
    
    figure(5),hold on,figname{5} = 'LLv';                   % L vs Lv
    plot(t, Lt(dnum,:),'DisplayName',dateAllstr{dnum})
    plot(t, Lvt(dnum,:),'DisplayName',dateAllstr{dnum})
    title('$L$ vs $L_v$','FontSize',18)
    xlabel('$t_s$','FontSize',18)
    
    figure(6),hold on,figname{6} = 'LLvlog';                % log L vs log Lv
    plot(log10(t), log10(Lt(dnum,:)),'DisplayName',dateAllstr{dnum})
    plot(log10(t), log10(Lvt(dnum,:)),'DisplayName',dateAllstr{dnum})
    title('$\log{L}$ vs $\log{L_v}$','FontSize',18)
    xlabel('$\log{t_s}$','FontSize',18)
    xlim([1.5 log10(t_max)])
    
    figure(7),hold on,figname{7} = 'allL';                   % L & Lv + avg
    plot(t, Lt(dnum,:),'-.','DisplayName',dateAllstr{dnum});
    plot(t, Lvt(dnum,:),':','DisplayName',dateAllstr{dnum});
    title('$L$ vs $L_v$','FontSize',18)
    xlabel('$t_s$','FontSize',18)
    
    figure(8),hold on,figname{8} = 'allLlog';                % log L vs log Lv + avg
    plot(log10(t), log10(Lt(dnum,:)),'-.','DisplayName',dateAllstr{dnum});
    plot(log10(t), log10(Lvt(dnum,:)),':','DisplayName',dateAllstr{dnum});
    title('$\log{L}$ vs $\log{L_v}$','FontSize',18)
    xlabel('$\log{t_s}$','FontSize',18)
    xlim([1.5 log10(t_max)])
end
% figure(9),hold on,figname{9} = 'avgL';                      % avg L
% plot(t, mean(Lt))
% title('avg $L$','FontSize',18)
% xlabel('$t_s$','FontSize',18)
%
% figure(10),hold on,figname{10} = 'avgLlog';                   % avg log L
% plot(log10(t), log10(mean(Lt)))
% title('avg $\log{L}$','FontSize',18)
% xlabel('$\log{t_s}$','FontSize',18)
%
% figure(11),hold on,figname{11} = 'avgLv';                     % avg Lv
% plot(t, mean(Lvt))
% title('avg $L_v$','FontSize',18)
% xlabel('$t_s$','FontSize',18)
%
% figure(12),hold on,figname{12} = 'avgLvlog';                % avg log Lv
% plot(log10(t), log10(mean(Lvt)))
% title('avg $\log{L_v}$','FontSize',18)
% xlabel('$\log{t_s}$','FontSize',18)

figure(13),hold on,figname{13} = 'avgLLv';                  % avg L vs avg Lv
plot(t, mean(Lt),'DisplayName','$L$')
plot(t, mean(Lvt),'DisplayName','$L_v$')
title('avg $L$ vs avg $L_v$','FontSize',18)
xlabel('$t_s$','FontSize',18)

figure(14),hold on,figname{14} = 'avgLLvlog';               % avg log L vs avg log Lv
plot(log10(t), log10(mean(Lt)),'DisplayName','$\log{L}$')
plot(log10(t), log10(mean(Lvt)),'DisplayName','$\log{L_v}$')
title('avg $\log{L}$ vs avg $\log{L_v}$','FontSize',18)
xlabel('$\log{t_s}$','FontSize',18)
xlim([1.5 log10(t_max)])

figure(7),hold on
plot(t, mean(Lt),'k','DisplayName','mean $L$');
plot(t, mean(Lvt),'k','DisplayName','mean $L_v$');

figure(8),hold on
plot(log10(t), log10(mean(Lt)),'k','DisplayName','mean $\log{L}$');
plot(log10(t), log10(mean(Lvt)),'k','DisplayName','mean $\log{L_v}$');

% % apply legend and params text
for fnum = 1:14
    if ishandle(fnum)
        figure(fnum);
        set(gcf,'name',figname{fnum})
        legend show
        set(legend,'interpreter','latex','box','off','location','northwest','FontSize',8)
        
        
        dateStr = strrep(dateAll{1},'_','\_');
        axes('Position',[0 0 1 1],'Visible','off');
        text(0.1,0.02,sprintf('%sr%d: $n_0=%.1f$, $(X,Y)=(%0.f,%0.f)$, $(N_x,N_y)=(%0d,%0d)$, $q=%.2f$, $c_0=%.2f$, $c_1=%.2f$, $t_\\mathrm{step}=%.2g$, $\\Delta t=%.2g$', ...
            dateStr, inputs.knum, inputs.n0, grids.maxX, grids.maxY, grids.N_x, grids.N_y, inputs.q, inputs.c_0, inputs.c_1, inputs.step_size, inputs.dt),'FontSize',18)
        
    end
end

%% save figs
% fnum = length(findobj('type','figure'));

for fnum = 1:14
    if ishandle(fnum)
        figure(fnum);
        print('-djpeg',sprintf(['figs/plots/' runname '/' figname{fnum}]))
    end
end