function plotgrowth(currdate)

load(['figs/' currdate '/inputs.mat'])
load(['figs/' currdate '/curdata.mat'])

tOK = ~isnan(Fz2t);
tuse = inputs.t(tOK);
close all;

plot(tuse,align2t(tOK),'r',tuse,Fz2t(tOK),'k',tuse,Fperp2t(tOK),'b')
hold on
if exist('alignperp2t', 'var')
    plot(tuse,alignperp2t(tOK),'m',tuse,alignperpunc2t(tOK),'color',0.5*[1 1 1],'linewidth',1.5)
end
xlim([0 max(tuse)])
xlabel('$t/t_s$')
assert(inputs.c_1==1)
xlabel('$t/t_s$')
ylabel('$\langle O^2\rangle/n_c^2$')
text(9.5,0.75,'$\langle\mathcal{A}^2\rangle$','color',[1 0 0])
text(15,0.09,'$\langle F_z^2\rangle$')
text(15,0.24,'$\langle F_\perp^2\rangle$','color',[0 0 1])

ylim([0 1.02])

grids = inputs.grids;

axes('Position',[0 0 1 1],'Visible','off');
text(0.1,0.98,sprintf('$n_0=%.1f$, $(X,Y)=(%0.f,%0.f)$, $(N_x,N_y)=(%0d,%0d)$, $q=%.2f$, $c_0=%.2f$, $c_1=%.2f$', inputs.n0, grids.maxX, grids.maxY, grids.N_x, grids.N_y, inputs.q, inputs.c_0, inputs.c_1),'FontSize',12)
if exist('timetaken', 'var')
    tstr = sprintf('time taken = $%.1f$hr, time taken/step = $%.0f$ms, GPU=%d, $10^4\\Delta t=%.2f$', timetaken/60^2, 1000*timetaken/(max(tuse)/inputs.dt), inputs.gpuUsed, 10^4*inputs.dt);
else
    tstr = sprintf('$10^4\\Delta t=%.2f$', 10^4*inputs.dt);
end

text(0.1,0.02,tstr,'FontSize',12)



max(tuse)

set(gcf, 'PaperOrientation', 'landscape')
print('-dpdf',sprintf('growth%s', currdate))