function rv = xquadspin(igrand,grids)
assert(iscell(igrand))
rv = nan(size(igrand));
for cnum = 1:numel(igrand)
    rv(cnum) = xquad(abs(igrand{cnum}).^2,grids);
end