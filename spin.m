function [Sx, Sy, Sz, Snu, Fnematic] = spin(f, xi)

m = f:-1:-f;
fmm = sqrt((f-m(1:end-1)+1).*(f+m(1:end-1)));
fmp = sqrt((f+m(2:end)+1).*(f-m(2:end)));
Sx = (diag(fmm,1) + diag(fmp,-1))/2;
Sy = (diag(fmm,1) - diag(fmp,-1))/(2i);
Sz = diag(m);

Snu = cell(4,1);
Snu{1}=Sx;
Snu{2}=Sy;
Snu{3}=Sz;
Snu{4}=eye(2*f+1);

if nargout > 4
    Fnematic = zeros(2*f+1,2*f+1,4,4);
    for ri = 1:4
        for rj = 1:4
            Fnematic(:,:,ri,rj) = (Snu{ri}*Snu{rj} + Snu{rj}*Snu{ri})/2;
        end
    end
end

if nargin > 1
    if ndims(xi) > 2
        sxi = size(xi);
        xi = reshape(xi,[],sxi(end));
        spacedims = sxi(1:end-1);
        spacen = prod(spacedims);
        Sx = zeros(spacedims);
        Sy = zeros(spacedims);
        Sz = zeros(spacedims);
        nemstore = zeros(spacen,4,4);
        for xnum = 1:spacen
            xix = xi(xnum,:).';
            [Sx(xnum), Sy(xnum), Sz(xnum), Snuthis, nemstore(xnum,:,:)] = prodSxi(xix,Snu,Fnematic);
        end
        Fnematic = reshape(nemstore,[spacedims 4 4]);
    else
        if nargout > 4
            [Sx, Sy, Sz, Snu, Fnematic] = prodSxi(xi,Snu,Fnematic);
        else
            [Sx, Sy, Sz, Snu] = prodSxi(xi,Snu);
        end
    end
end

function [Sx, Sy, Sz, Snu, Fnematic] = prodSxi(xi,Snu,Fnematic)
if nargout > 4
    nemstore = zeros(4,4);
    for ri = 1:4
        for rj = 1:4
            nemstore(ri,rj) = xi'*Fnematic(:,:,ri,rj)*xi;
        end
    end
    Fnematic = nemstore;
end
for ri = 1:4
    Snu{ri} = xi'*Snu{ri}*xi;
end
Sx = Snu{1};
Sy = Snu{2};
Sz = Snu{3};
