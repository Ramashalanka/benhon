function thestruct = makestruct(varargin)
% for 2015b and later must put normal struct type args (in quotes) at the end
vnum = 1;
while vnum <= nargin
    thename = inputname(vnum);
    if isempty(thename) % strcmp(thename, '')
        thename = varargin{vnum};
        vnum = vnum + 1;
    end
    theval = varargin{vnum};
    thestruct.(thename) = theval;
    vnum = vnum + 1;
end

