%% AF Energies
% Plot Bogoliubov energy spectra for AF regime.
close all
clear
set(0,'defaulttextInterpreter','latex');

% parameters
c_0 = 3;
c_1 = 1;
n = 1;
k = linspace(0,10,1e5);
ek = k.^2/2;

% choose output plot
contplot = false; % false: discrete q, true: 'continuous' q

if ~contplot
    qlist = [0.5 2 2.5 3 5 8]; % values of q to plot
else
    qlist = 0:0.05:2; % values of q for 'continuous' q plot
end
qstyle = 'rbkgcm'; % q colours

for qnum = 1:numel(qlist)
    lgnames{qnum} = sprintf('$q=%.1f$',qlist(qnum)); % legend label
end

E2_plus = @(ek) ek.*(ek+(2*c_0*n));
E2_minus = @(ek,q) ek.*(ek+(2*c_1*n));
E2_zero = @(ek,q) (ek-q).*(ek-q+(2*c_1*n));
yl = [0 10];
xl = [0 5];

figure(1)
set(gcf,'position', [1          41        1920         963])
for qnum = 1:numel(qlist)
    qtest = qlist(qnum);
    qstylei = mod(qnum,numel(qstyle))+1;
    
    subplot(1,2,1)
    hold on
    y2 = E2_zero(ek,qtest);
    yreal = y2;
    ekreal = y2 > 0;
    yreal(~ekreal) = NaN;
    kall = k;
    kall(~ekreal) = NaN;
    plot(kall,sqrt(yreal),qstyle(qstylei)); % Re(E)
    plot(k(~ekreal),sqrt(-y2(~ekreal)),['--' qstyle(qstylei)]); % Im(E)
    title('$E_{\mathbf{k},0}$','FontSize',18)
    xlabel('$k$','FontSize',18)
    if ~contplot
        ylim(yl)
        xlim(xl)
    else
        ylim([0 1.1]) % cont zoom
        xlim([0 2.1]) % cont zoom
    end
    hlg1(qnum) = plot(NaN,NaN,qstyle(qstylei),'DisplayName',lgnames{1,qnum}); % legend title
    
    subplot(1,2,2)
    hold on
    if ~contplot
        plot(k,sqrt(E2_plus(ek)),'r')
        plot(k,sqrt(E2_minus(ek)),'b')
        title('$E_{\mathbf{k},+/-}$','FontSize',18)
        xlabel('$k$','FontSize',18)
        ylim(yl)
        xlim(xl)
    else
        plot(qtest,max(sqrt(-y2(~ekreal))),'x')
        plot([1 1],[0 1],'k:')
        title('$\mathrm{max(Im}(E_{\mathbf{k},0}))$','FontSize',18)
        xlabel('$q$','FontSize',18)
    end
end

% make legends
subplot(1,2,1)
hlg1(numel(qlist)+1) = plot(NaN,NaN,'k-','DisplayName','$\mathrm{Re}(E_\mathbf{k})$');
hlg1(numel(qlist)+2) = plot(NaN,NaN,'k--','DisplayName','$\mathrm{Im}(E_\mathbf{k})$');
if ~contplot
    legend(hlg1);
    set(legend,'interpreter','latex','box','off','location','northwest','fontsize',18)

    subplot(1,2,2)
    hlg2(1) = plot(NaN,NaN,'r','DisplayName','$E_{\mathbf{k},+}$');
    hlg2(2) = plot(NaN,NaN,'b','DisplayName','$E_{\mathbf{k},-}$');
    legend(hlg2);
    set(legend,'interpreter','latex','box','off','location','northwest','fontsize',18)
    
    axes('Position',[0 0 1 1],'Visible','off');
    text(0.1,0.02,sprintf('$\\mathrm{Parameters:}$ $n=%.1f$, $c_0=%.2f$, $c_1=%.2f$', n, c_0, c_1),'FontSize',18)
    print('-djpeg',sprintf('figs/plots/energies/BDGspectra'))
else
    axes('Position',[0 0 1 1],'Visible','off');
    text(0.1,0.02,sprintf('$\\mathrm{Parameters:}$ $n=%.1f$, $c_0=%.2f$, $c_1=%.2f$, $q=%g:%g:%g$', n, c_0, c_1, min(qlist), qlist(2)-qlist(1), max(qlist)),'FontSize',18)
    print('-djpeg',sprintf('figs/plots/energies/BDGspectracont'))
end

return
%{
figure();
set(gcf,'position', [1          41        1920         963])
for qnum = 1:numel(qlist)
    qtest = qlist(qnum);
    subplot(1,3,1)
    hold on
    plot(k,sqrt(E2_plus(ek)),qstyle(qnum))
    title('$E_{\mathbf{k},+}$','FontSize',18)
    xlabel('$k$')
    ylim(yl)
    xlim(xl)
    
    subplot(1,3,2)
    hold on
    y2 = E2_zero(ek,qtest);
    yreal = y2;
    ekreal = y2 > 0;
    yreal(~ekreal) = NaN;
    kall = k;
    kall(~ekreal) = NaN;
    plot(kall,sqrt(yreal),qstyle(qnum));%,'DisplayName',lgnames{1,qnum})
    plot(k(~ekreal),sqrt(-y2(~ekreal)),['--' qstyle(qnum)]);%,'DisplayName',lgnames{2,qnum})
    %plot(ek,zeros(size(ek)))
    title('$E_{\mathbf{k},0}$','FontSize',18)
    xlabel('$k$')
    ylim(yl)
    xlim(xl)
    legend show
    set(legend,'interpreter','latex','box','off','location','northwest','fontsize',18)
    
    subplot(1,3,3)
    hold on
    plot(k,sqrt(E2_minus(ek)),qstyle(qnum))
    title('$E_{\mathbf{k},-}$','FontSize',18)
    xlabel('$k$')
    ylim(yl)
    xlim(xl)
end
%}