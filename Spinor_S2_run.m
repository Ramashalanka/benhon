% S2 - second-order symplectic method (leapfrog)
%
% Time-evolves psi etc.
% Runs using the params file created by the main Spinor_S2 file,
% using inputs specified there.
function Spinor_S2_run(currdate, knum)
close all;
set(0,'defaulttextInterpreter','latex');
dbstop if error;

paramname = sprintf('figs/%s/params%s', currdate, currdate);
pload = load(paramname);
inputs = pload.inputs;

gi = inputs.gi;
maxX = gi.maxX;
maxY = gi.maxY;
N_x = gi.N_x;
N_y = gi.N_y;

% Define wavefunction parameters
rng('shuffle')
psi = cell(3,1); % 1, 0, -1
for cnum = 1:3
    psi{cnum} = (randn(N_x,N_y) + 1i*randn(N_x,N_y))*sqrt(inputs.n0)/25; % random noise
end
% psi{2} = psi{2} + sqrt(n0)*ones(N_x,N_y); % polar
for cnum = [1 3] % AF
    psi{cnum} = psi{cnum} + sqrt(inputs.n0/2)*ones(N_x,N_y);
end

% find name of machine
[ret, hostname] = system('hostname');
if ret ~= 0
    if ispc
        hostname = getenv('COMPUTERNAME');
    else
        hostname = getenv('HOSTNAME');
    end
end
hostname = strtrim(lower(hostname));
inputs.hostname = hostname;

timestart = stime;

% create run name (e.g. r3) for output directory automatically if unspecified
if nargin < 2
    knum = 1;
    while exist(sprintf('figs/%s/r%d', currdate, knum), 'dir')
        knum = knum + 1;
    end
end
runpath = sprintf('figs/%s/r%d/', currdate, knum);
mkdir(runpath)

inputs.knum = knum;
inputs.runpath = runpath;
mkdir([runpath 'data'])
mkdir([runpath 'pspin'])

% print major input parameters
infostr = sprintf('%sr%d: N=(%5d,%5d), R=(%6.1f,%6.1f), q=%3.1f, dt=%.6f, s=(%3.2f,%3.2f,%3.2f), host=%s\n', currdate, knum, N_x, N_y, maxX, maxY, inputs.q, inputs.dt, inputs.sigma_v, hostname);
stepstr = sprintf('plotstep = %6d, psistep = %6d\n', inputs.plotstep, inputs.psistep);
fprintf([infostr stepstr]);
% save main parameters to file with date
fout = fopen('figs/rundetails.txt', 'a');
fprintf(fout, infostr);
fclose(fout);


[x,dx1,kx,dkx] = vargrid(maxX,N_x);
[y,dy1,ky,dky] = vargrid(maxY,N_y);

dr = dx1*dy1;
dkr = dkx*dky;
[X,Y] = ndgrid(x,y);
R = sqrt(X.^2+Y.^2);

% sigma = 3;
% psi{1} = 5*exp(-R.^2/(2*sigma^2))/(pi*sigma^2)^(1/2);
% psi{2} = psi{1};
% psi{3} = 7/5*psi{1};

[KX,KY] = ndgrid(kx,ky);
Ksqd2 = (KX.^2 + KY.^2)/2;
sEk = fftshift(Ksqd2);
Vpot = 0.5.*(inputs.omega(1)^2*X.^2+inputs.omega(2)^2*Y.^2); % harmonic trap (unused)

gridsCPU = makestruct(maxX,maxY,N_x,N_y,x,y,X,Y,R,Vpot,Ksqd2,sEk,dr,dkr,'fttype',-1);
step_size = inputs.step_size;
t = (0:step_size:inputs.t_max)';
inputs.t = t;
inputs.N0 = 4*maxX*maxY*inputs.n0;
inputs.N0sq = inputs.N0*inputs.n0;
inputs.ninitial = sum(xquadspin(psi,gridsCPU)/inputs.N0); % sum(allavn(:,1))
inputs.grids = gridsCPU;

if inputs.usegpu && gpuDeviceCount > 0
    grids = makestruct(maxX,maxY,N_x,N_y,dr,dkr,'fttype',-1);
    grids.x = gpuArray(x);
    grids.y = gpuArray(y);
    grids.X = gpuArray(X);
    grids.Y = gpuArray(Y);
    grids.R = gpuArray(R);
    grids.Vpot = gpuArray(Vpot);
    grids.Ksqd2 = gpuArray(Ksqd2);
    grids.sEk = gpuArray(sEk);
    inputs.gpuUsed = true;
    psiCPU = psi;
    psi = cell(3,1);
    for cnum = 1:3
        psi{cnum} = gpuArray(psiCPU{cnum});
    end
    fprintf('using GPU\n')
else
    grids = gridsCPU;
    inputs.gpuUsed = false;
    fprintf('no GPU\n')
end
grids.r = R(:);
inputs.grids.r = grids.r;
psit = psi;

% preallocate
dtime = struct();%'allavn', [],'Fz2t', [], 'Fperp2t', [], 'align2t', [], 'alignperp2t', [], 'alignperpunc2t', [], 'Lt', [], 'vnumt', []);
dataall = calcdata(psit,inputs);

% initial values for dtime
data = dataall.data;
dtime = makedtime(dtime,data,1);
inputs.Einitial = data.Etotal;

% SAVE INPUTS
save(sprintf('%s/data/inputs', runpath), 'inputs') 
inputs.grids = grids; % overwrite gridsCPU if GPU

% initial plot (plotspinangle uses figure(1))
plotspinangle(psit,t,dataall,dtime,inputs,0);

% TIME EVOLUTION ALGORITHM
%fprintf('%d:', length(t)-1)
%reverseStr = '';
steptime = tic;
for ti=1:(length(t)-1)
    %msg = sprintf('%d',ti);
    %fprintf([reverseStr, msg]);
    %reverseStr = repmat(sprintf('\b'), 1, length(msg));
    
    psit = stepS2(psit, inputs); % S2
    
    % increment dtime
    dataall = calcdata(psit,inputs);
    data = dataall.data;
    dtime = makedtime(dtime,data,ti+1);
    
    % timing
    pctdone = 100*ti/length(t); % percentage completion of run (max ti = t_max/dt)
    timetaken = stime-timestart;
    eta = timetaken*((length(t)/ti)-1); % linear guess at time remaining
    
    % print current step info
    fprintf('%s %.2fs %sr%d:q=%.2f,dt=%.5f,(X,Y)=(%.0f,%.0f),N=(%d,%d),ti=%d/%d (%.1f%%,ETA=%s),n_err=%.2f,E_err=%.2f,L=%.1f,Lv=(%.1f,%.1f,%.1f)\n', ...
        datestr(now,'HH:MM:SS'), toc(steptime), currdate, knum, inputs.q, inputs.dt, maxX, maxY, N_x, N_y, ti, length(t), pctdone, duration(0,0,eta), ...
        log10(abs(inputs.ninitial-sum(data.Nv))), log10(abs(inputs.Einitial/data.Etotal-1)), data.Lpos(2), data.Lv);
    
    if ~mod(ti,inputs.plotstep) % save every (plotstep)th timestep to file
        save(sprintf('%s/data/timedata', runpath), 'dtime', 'timetaken')
        plotspinangle(psit,t,dataall,dtime,inputs,ti); % create figures/plots
        plotdata = dataall.plotdata;
        save(sprintf('%s/data/plotdata%04d', runpath, ti/inputs.plotstep), 'plotdata', 'ti')
    end
    if ~mod(ti,inputs.psistep) % save psi every (psistep)th timestep to file
        % save(sprintf('%s/data/psi%04d', runpath, ti/inputs.plotstep), 'psit', 'ti')       % psit: GPU Array
        psitCPU.psit = cellfun(@gather,psit,'UniformOutput',false);
        psitCPU.ti = ti;
        save(sprintf('%s/data/psi%04d', runpath, ti/inputs.plotstep),'-struct','psitCPU')   % psit: CPU
        clear psitCPU % save memory?
    end
    steptime = tic;
end
% output total time taken
fprintf('\ntime taken: %s\n',duration(0,0,timetaken))

% save time taken to file with date
infostr = sprintf('%sr%d: %s %s\n', currdate, knum, duration(0,0,timetaken), hostname);
fout = fopen('figs/runtimes.txt', 'a');
fprintf(fout, infostr);
fclose(fout);