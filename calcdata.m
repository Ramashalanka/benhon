function dataall = calcdata(psit,inputs)
grids = inputs.grids;

%[Fx, Fy, Fz, Fnu, Fnematic] = spin(1);
n = cell(3,1);
ntotal = 0;
for mnum = 1:3
    n{mnum} = abs(psit{mnum}.').^2; % n as a function of position
    ntotal = ntotal + n{mnum};
end

Nv=xquadspin(psit,grids)/inputs.N0;
avn = xquadspin(psit,grids)/inputs.N0; % these are the same - get rid of one?

EK = nan(3,1);
for m = 1:-1:-1
    mnum = 2-m;
    phitm = fftshift(fftn(ifftshift(psit{mnum})))*grids.dr;
    npm = abs(phitm).^2;
    igrand = grids.Ksqd2.*npm;
    EK(mnum) = gather(sum(igrand(:)))*grids.dkr/(2*pi)^2;
    EQ(mnum) = inputs.q*m^2*Nv(mnum)*inputs.N0;
    if m == 0
        n1r = gather(fftshift(ifftn(ifftshift(npm)))/grids.dr);
        EK0 = EK(mnum);
    end
end
EC0 = inputs.c_0/2*gather(xquad(ntotal.^2,grids));

Fz = gather(abs(psit{1}).^2 - abs(psit{3}).^2);
Fperp = gather(sqrt(2)*(conj(psit{1}).*psit{2} + conj(psit{2}).*psit{3}));

alignall = gather(abs(psit{2}.^2 - 2*psit{1}.*psit{3}));
alignperpall = gather(abs(-2*psit{1}.*psit{3}));
alignperpuncall = gather(4*abs(psit{1}).^2.*abs(psit{3}).^2);

Fz2 = xquad(Fz.^2,grids)/inputs.N0sq;
Fperp2 = xquad(abs(Fperp).^2,grids)/inputs.N0sq;
EC1 = inputs.c_1/2*(Fperp2+Fz2)*inputs.N0sq;
Etotal = sum(EK) + sum(EQ) + EC0 + EC1;


align2 = xquad(alignall.^2,grids)/inputs.N0sq;
alignperp2 = xquad(alignperpall.^2,grids)/inputs.N0sq;
alignperpunc2 = xquad(alignperpuncall,grids)/inputs.N0sq;

% correlation function binning
edges = 0:inputs.binsize:max(grids.r)+inputs.binsize;
yIm = imag(n1r(:))/Nv(2)/inputs.N0;
yRe = real(n1r(:))/Nv(2)/inputs.N0;
[~,~,idx] = histcounts(grids.r,edges); % B: # in each bin; idx: bin index for each value of x
rmean = accumarray(idx(:),grids.r,[],@mean);
yImmean = accumarray(idx(:),yIm,[],@mean);
yRemean = accumarray(idx(:),yRe,[],@mean);
for count = 1:length(rmean)
    if rmean(count) == 0 && yRemean(count) == 0
        rmean(count) = nan;
    end
end
index = isnan(rmean);
rmean(index) = [];
yRemean(index) = [];
yImmean(index) = [];
[~,maxpos] = max(yRemean);
assert(maxpos == 1)
Lvalues = [0 inputs.Ly*maxpos];
Lpos = nan(2,1);
for Lchk = 1:2
    ylesspos = find(yRemean<Lvalues(Lchk),1,'first');
    if(~isempty(ylesspos) && ylesspos > 1) % if no intercept, leave Lpos as nan
        Lpos(Lchk) = interp1(yRemean(ylesspos-1:ylesspos),rmean(ylesspos-1:ylesspos),Lvalues(Lchk),'linear');
    end
end

% VORTEX DETECTION
% preallocate
snum = numel(inputs.sigma_v);
% vpos = struct();
vnum = nan(snum,2);
Lv = nan(1,snum);
for si = 1:snum % note si = [1 2 3 ...], while sigma_v = [2 1 5 ...]
    vpos(si) = structfun(@gather,findvortex(psit,inputs,si),'UniformOutput',false); % find vortex coordinates
    vnum(si,:) = [numel(vpos(si).vpx), numel(vpos(si).vnx)]; % count vortex number
    Lv(1,si) = sqrt(4*grids.maxX*grids.maxY/sum(vnum(si,:))); % inter-vortex length
end


plotdata = makestruct(rmean,yImmean,yRemean,vpos);%,splinefit);
data = makestruct(Nv,avn,EK0,EK,EQ,EC0,EC1,Etotal,Fz2,Fperp2,align2,alignperp2,alignperpunc2,Lpos,vnum,Lv); % small stuff
dataall = makestruct(n,n1r,Fz,Fperp,alignall,alignperpall,alignperpuncall,data,plotdata); % big (grid) stuff + the other structs








