%load figs/2021-08-05_16-59-34/data/psi2230
%load figs/2021-08-05_16-59-34/data/inputs
close all;clear
set(0,'defaulttextInterpreter','latex');

fname = 'hsv';
load figs/tmp/psi1990CPU
load figs/tmp/inputs
grids = inputs.grids;

s = [1 2 5];
ls = numel(s);

figure(1)
set(gcf,'position', [1 41 1920 963])
subplot(2,ls+1,1)
imagesc(abs(psit{2}).^2)
set(gca,'ydir','normal')
axis(gca,'image','tight')
colormap(gca,'hsv')
colorbar

subplot(2,ls+1,ls+2)
phase = angle(psit{2});
imagesc(grids.x,grids.y,phase.');
axis(gca,'image','tight')
set(gca,'ydir','normal','clim',[-pi,pi])
colormap(gca,'hsv')
colorbar

for ii = 1:ls
    % tic
    
    % fbr = exp(-inputs.grids.R.^2/(2*s(ii)^2))/(2*pi*s(ii)^2);
    % xquad(fbr,inputs.grids)
    fblurk = exp(-s(ii)^2*inputs.grids.Ksqd2);
    psik = fftshift(fftn(ifftshift(psit{2})));
    psiblur = fftshift(ifftn(ifftshift(fblurk.*psik)));
    % xquad(psit{2},inputs.grids)
    % xquad(psiblur,inputs.grids)
    
    % fprintf('s=%.0f: %f, ',s(ii),toc)
    
    figure(1)
    subplot(2,ls+1,ii+1)
    imagesc(abs(psiblur).^2)
    axis(gca,'image','tight')
    set(gca,'ydir','normal')
    colormap(gca,'hsv')
    colorbar
    title(sprintf('$s=%.3f$', s(ii)),'FontSize',18)
    
    subplot(2,ls+1,ls+2+ii)
    phase = angle(psiblur);
    imagesc(grids.x,grids.y,phase.');
    axis(gca,'image','tight')
    set(gca,'ydir','normal','clim',[-pi,pi])
    colormap(gca,'hsv')
    colorbar
    title(sprintf('s=%.3f', s(ii)),'FontSize',18)
    
    if ii == ls
        print('-dpng',sprintf('figs/plots/blur/%sblur(%.0f,%.0f,%.0f)', fname, s))
    end
    
    % tic
    
    figure
    set(gcf,'position', [1 41 1920 963])
    subplot(1,2,1)
    phase = angle(psit{2});
    imagesc(grids.x,grids.y,phase.');
    hold on;
    set(gca,'ydir','normal','clim',[-pi,pi])
    axis(gca,'image','tight')
    colormap(gca,'hsv')
    colorbar
    vpos = findvortex(psit,inputs);
    scatter(grids.x(vpos.vpx),grids.y(vpos.vpy),50,'k+','linewidth',1)
    scatter(grids.x(vpos.vnx),grids.y(vpos.vny),50,'ko','linewidth',1)
    vnum = [numel(vpos.vpx), numel(vpos.vnx)]; % count vortex number
    title(sprintf('Before blur: $v_{\\mathrm{tot}}=%d$', sum(vnum)),'FontSize',18)
    
    subplot(1,2,2)
    psib{2} = psiblur;
    phase = angle(psib{2});
    imagesc(grids.x,grids.y,phase.');
    hold on;
    set(gca,'ydir','normal','clim',[-pi,pi])
    axis(gca,'image','tight')
    colormap(gca,'hsv')
    colorbar
    vposb = findvortex(psib,inputs);
    scatter(grids.x(vposb.vpx),grids.y(vposb.vpy),50,'k+','linewidth',1)
    scatter(grids.x(vposb.vnx),grids.y(vposb.vny),50,'ko','linewidth',1)
    vnumb = [numel(vposb.vpx), numel(vposb.vnx)]; % count vortex number
    title(sprintf('After blur: $s=%.3f, v_{\\mathrm{tot}}=%d$', s(ii), sum(vnumb)),'FontSize',18)
    
    % fprintf('vplot: %f\n',toc)
    %%% s=1: 0.044386, vplot: 0.604723
    %%% s=2: 0.046548, vplot: 0.597161
    %%% s=5: 0.046457, vplot: 0.606652
    
    
    print('-dpng',sprintf('figs/plots/blur/vblur%.0f',s(ii)))
    
end