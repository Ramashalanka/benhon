function plotrun(dateStr)
% plots:
% L & L_v (with averaging and comparison)

% to-do:
% error check without needing to compare against dateStr manually
% robust to choice of number of collapsed correlation functions (even spacing)

close all
set(0,'defaulttextInterpreter','latex');
dbstop if error;
cleanupObj = onCleanup(@()rehash);

dirInfo = dir('figs/2*');
dirDateStrs = cellstr(cat(1,dirInfo.name)); % collects existing dateStr directory names in cell array

if nargin < 1
    fprintf('Available runs:\n')
    dateStr = input(sprintf('Select date string index: '),'s');
    return
elseif nargin == 1
    fprintf('1')
    return
end

dir('figs/2*')
dateFormat = 'yyyy-mm-dd_HH-MM-SS';
datenumStart = datenum('2021-09-07_20-24-42',dateFormat);


% manually add strings (can scrap this section later for ease of use)
savedStrs = {'2021-09-07_20-24-42','2021-09-09_20-00-13','2021-09-12_17-19-17'};
switch dateStr % name for output folder - could just name by dateStr
    case savedStrs{1} % '2021-09-07_20-24-42' q=2.5
        runidx = 8;
    case savedStrs{2} % '2021-09-09_20-00-13' q=2.5 (same params as run8 by mistake)
        runidx = 9;
    case  savedStrs{3} % '2021-09-09_20-00-13' q=5
        runidx = 10;
    otherwise
        fprintf('Not a valid run-title/date-string [e.g. plotrun(''2021-09-07_20-24-42'') or plotrun(''2021-09-07_20-24-42'',4)].\n')
        return
end

dateStrTex = strrep(dateStr,'_','\_');
runname = sprintf(['run' num2str(runidx)]); % sets output folder e.g. 'run5'
[~,~] = mkdir('figs',['plots/' runname]); % makes output directory e.g. 'figs/plots/run5'

% to-do: check if there is a data directory corresponding to the dateStr
% outpath = sprintf('figs/%s/plots',dateStr);
% [~,~] = mkdir(outpath);

runcount = numel(dir(sprintf('figs/%s/r*',dateStr)));

% preallocate
figname = {};
% St = ???
% Si = ???
Lt = cell(1,runcount);
Lvt = cell(1,runcount);
iuse = cell(1,runcount);
shotname = cell(1,runcount);
t = cell(1,runcount);
for dnum = 1:runcount % load data
    datapath = sprintf('figs/%s/r%d/data/', dateStr, dnum); % folder structure e.g. 'figs/2021-09-07_20-24-42/r2/data/'
    St(dnum) = load([datapath 'timedata.mat']);
    Si(dnum) = load([datapath 'inputs.mat']);
    Lt{dnum} = St(dnum).dtime.Lt(2,:);
    Lvt{dnum} = St(dnum).dtime.Lvt(1,:); % only for first sigma_v blurring
    iuse{dnum} = ones(1,length(St(dnum).dtime.Lt));
    shotname{dnum} = sprintf('r%d',dnum);
    % t{dnum} = Si(dnum).inputs.t;
end
fprintf('Data loaded...\n')

% pad unfinished run vectors with NaNs
maxlen = max(cellfun('prodofsize',Lt));
minlen = min(cellfun('prodofsize',Lt));
if ~(minlen == maxlen) % if there are unfinished shots remaining
    for dnum = 1:runcount
        current_elem=numel(Lt{dnum});
        if current_elem<maxlen
            Lt{dnum}((current_elem+1):maxlen)=NaN;
            Lvt{dnum}((current_elem+1):maxlen)=NaN;
        end
    end
    fprintf('Unfinished shots padded...\n')
else
    fprintf('No unfinished shots found...\n')
end

for dnum = 1:runcount % plot
    t{dnum} = Si(dnum).inputs.t(1:maxlen);
    t_max = Si(dnum).inputs.t_max;
    
    % figure(1),hold on,figname{1} = 'L';                     % L
    % plot(Si(dnum).inputs.t, Lt{dnum},'DisplayName',shotname{dnum})%,'linewidth',2)
    % title('$L$','FontSize',18)
    % xlabel('$t/t_s$','FontSize',18)
    %
    % figure(2),hold on,figname{2} = 'Llog';                  % log(L)
    % plot(log10(Si(dnum).inputs.t), log10(Lt{dnum}),'DisplayName',shotname{dnum})
    % title('$\log_{10}{L}$','FontSize',18)
    % xlabel('$\log_{10}{t/t_s}$','FontSize',18)
    %
    % figure(3),hold on,figname{3} = 'Lv';                    % Lv
    % plot(Si(dnum).inputs.t, Lvt{dnum},'DisplayName',shotname{dnum})
    % title('$L_v$','FontSize',18)
    % xlabel('$t/t_s$','FontSize',18)
    %
    % figure(4),hold on,figname{4} = 'Lvlog';                 % log(Lv)
    % plot(log10(Si(dnum).inputs.t), log10(Lvt{dnum}),'DisplayName',shotname{dnum})
    % title('$\log_{10}{L_v}$','FontSize',18)
    % xlabel('$\log_{10}{t/t_s}$','FontSize',18)
    
    figure(5),hold on,figname{5} = 'LLv';                   % L, Lv
    plot(t{dnum}, Lvt{dnum},'DisplayName',shotname{dnum})
    plot(t{dnum}, Lt{dnum},'DisplayName',shotname{dnum})
    title('$L$ vs $L_v$','FontSize',18)
    xlabel('$t/t_s$','FontSize',18)
    
    figure(6),hold on,figname{6} = 'LLvlog';                % log(L), log(Lv)
    plot(log10(t{dnum}), log10(Lvt{dnum}),'DisplayName',shotname{dnum})
    plot(log10(t{dnum}), log10(Lt{dnum}),'DisplayName',shotname{dnum})
    title('$\log_{10}{L}$ vs $\log_{10}{L_v}$','FontSize',18)
    xlabel('$\log_{10}{t/t_s}$','FontSize',18)
    xlim([1.5 log10(t_max)])
    
    figure(7),hold on,figname{7} = 'allL';                   % avg(L, Lv)
    plot(t{dnum}, Lvt{dnum},':','DisplayName',shotname{dnum});
    plot(t{dnum}, Lt{dnum},'-.','DisplayName',shotname{dnum});
    title('$L$ vs $L_v$','FontSize',18)
    xlabel('$t/t_s$','FontSize',18)
    
    figure(8),hold on,figname{8} = 'allLlog';                % avg(log(L), log(Lv))
    plot(log10(t{dnum}), log10(Lvt{dnum}),':','DisplayName',shotname{dnum});
    plot(log10(t{dnum}), log10(Lt{dnum}),'-.','DisplayName',shotname{dnum});
    title('$\log_{10}{L}$ vs $\log_{10}{L_v}$','FontSize',18)
    xlabel('$\log_{10}{t/t_s}$','FontSize',18)
    xlim([1.5 log10(t_max)])
end
fprintf('Loop plots complete...\n')
inputs = Si(1).inputs;
grids = inputs.grids;
t = t{1};

% figure(9),hold on,figname{9} = 'avgL';                      % avg(L)
% plot(t, mean(Lt))
% title('avg $L$','FontSize',18)
% xlabel('$t/t_s$','FontSize',18)
%
% figure(10),hold on,figname{10} = 'avgLlog';                   % avg(log(L))
% plot(log10(t), log10(mean(Lt)))
% title('avg $\log_{10}{L}$','FontSize',18)
% xlabel('$\log_{10}{t/t_s}$','FontSize',18)
%
% figure(11),hold on,figname{11} = 'avgLv';                     % avg(Lv)
% plot(t, mean(Lvt))
% title('avg $L_v$','FontSize',18)
% xlabel('$t/t_s$','FontSize',18)
%
% figure(12),hold on,figname{12} = 'avgLvlog';                % avg(log(Lv))
% plot(log10(t), log10(mean(Lvt)))
% title('avg $\log_{10}{L_v}$','FontSize',18)
% xlabel('$\log_{10}{t/t_s}$','FontSize',18)

figure(13),hold on,figname{13} = 'avgLLv';                  % avg(L), avg(Lv)
plot(t, cmean(Lvt),'DisplayName','$L_v$')
plot(t, cmean(Lt),'DisplayName','$L$')
title('avg $L$ vs avg $L_v$','FontSize',18)
xlabel('$t/t_s$','FontSize',18)

figure(14),hold on,figname{14} = 'avgLLvlog';               % avg(log(L)), avg(log(Lv))
plot(log10(t), log10(cmean(Lvt)),'DisplayName','$\log_{10}{L_v}$')
plot(log10(t), log10(cmean(Lt)),'DisplayName','$\log_{10}{L}$')
title('avg $\log_{10}{L}$ vs avg $\log_{10}{L_v}$','FontSize',18)
xlabel('$\log_{10}{t/t_s}$','FontSize',18)
xlim([1.5 log10(t_max)])

figure(7),hold on
plot(t, cmean(Lvt),'k','DisplayName','mean $L_v$');
plot(t, cmean(Lt),'k','DisplayName','mean $L$');

figure(8),hold on
plot(log10(t), log10(cmean(Lvt)),'k','DisplayName','mean $\log_{10}{L_v}$');
plot(log10(t), log10(cmean(Lt)),'k','DisplayName','mean $\log_{10}{L}$');

figarray = findobj('type','figure');
figids = cat(2,figarray.Number); % collects open figure numbers in column vector

% apply legend and params text
for fnum = figids
    if ishandle(fnum) % maybe unnecessary now
        figure(fnum);
        set(gcf,'position', [1 41 1920 963])
        % set(gcf,'name',figname{fnum})
        legend show
        set(legend,'interpreter','latex','box','off','location','northwest','FontSize',18)
        
        axes('Position',[0 0 1 1],'Visible','off');
        text(0.1,0.02,sprintf('%s: $n_0=%.1f$, $(X,Y)=(%0.f,%0.f)$, $(N_x,N_y)=(%0d,%0d)$, $q=%.2f$, $c_0=%.2f$, $c_1=%.2f$, $t_\\mathrm{step}=%.2g$, $\\Delta t=%.2g$', ...
            dateStrTex, inputs.n0, grids.maxX, grids.maxY, grids.N_x, grids.N_y, inputs.q, inputs.c_0, inputs.c_1, inputs.step_size, inputs.dt),'FontSize',18)
        
    end
end
fprintf('Run figures complete...\n')

% save figs
% fcount = length(findobj('type','figure'));
for fnum = figids
    if ishandle(fnum)
        figure(fnum);
        print('-djpeg',sprintf('figs/plots/%s/%s',runname,figname{fnum}))
    end
end
fprintf('Run figures saved.\n')

%% correlation function
% only proceed if all shots are finished
if ~(minlen == maxlen)
    fprintf('Correlation plots not genereated - unfinished shots remaining.\n')
    return
end
fprintf('Generating correlation collapse plots...\n')
close all
% set(0,'defaulttextInterpreter','latex');

[~,~] = mkdir(sprintf('figs/plots/%s/collapse',runname)); % makes output directory e.g. 'figs/plots/run5/collapse'

fprintf('Plotting shot number:')
reverseStr = '';
for dnum = 1:runcount
    msg = sprintf('%d...',dnum);
    fprintf([reverseStr, msg]);
    reverseStr = repmat(sprintf('\b'), 1, length(msg));
    
    h1 = figure(1);
    clf(h1),ax = axes();hold on
    set(gcf,'position', [1 41 1920 963])
    
    ccnum = 10; % number of evenly-spaced correlation functions to collapse
    cmap2 = turbo(ccnum);
    % initialise
    % pdata = ???
    Lpos = nan(1,ccnum);
    h = nan(1,ccnum);
    for step = 1:ccnum
        pdatapath = sprintf('figs/%s/r%d/data/plotdata%04d.mat', dateStr, dnum, step*100); % plotdata path e.g. 'figs/2021-09-07_20-24-42/r2/data/plotdata0500.mat'
        pdata(step) = load(pdatapath);
        Lpos(step) = Lt{dnum}(pdata(step).ti);
        h(step) = plot(pdata(step).plotdata.rmean./Lpos(step),pdata(step).plotdata.yRemean,'x',...
            'color',cmap2(step,:),...
            'MarkerSize',2,...
            'DisplayName',sprintf('$t=%d$ $t/t_s$',pdata(step).ti*inputs.step_size));
    end
    plot(1,inputs.Ly,'ro','DisplayName','$L$')
    
    % legend customisation
    hCopy = copyobj(h, ax);
    set(hCopy,'XData', NaN', 'YData', NaN)
    % arrayfun(@(h)set(h,'XData',nan(size(h.XData))),hCopy)
    for ii=1:numel(hCopy)
        hCopy(ii).MarkerSize = 15;
    end
    l = legend(hCopy);
    set(l,'interpreter','latex','box','off','location','northeast','FontSize',18)
    
    xlabel('$r/L$','FontSize',18)
    % ylabel('')
    title('$n^{(1)}_0(r)/N_0$','FontSize',18)
    % xlim([0 inputs.gi.maxX])
    xlim([0 14])
    ylim([-0.3 1])
    
    axes('Position',[0 0 1 1],'Visible','off');
    text(0.1,0.02,sprintf('%s: $n_0=%.1f$, $(X,Y)=(%0.f,%0.f)$, $(N_x,N_y)=(%0d,%0d)$, $q=%.2f$, $c_0=%.2f$, $c_1=%.2f$, $t_\\mathrm{step}=%.2g$, $\\Delta t=%.2g$, $\\sigma=%.2f$', ...
        dateStrTex, inputs.n0, grids.maxX, grids.maxY, grids.N_x, grids.N_y, inputs.q, inputs.c_0, inputs.c_1, inputs.step_size, inputs.dt, inputs.sigma_v(1)),'FontSize',18)
    
    print('-djpeg',sprintf('figs/plots/%s/collapse/collapse-s%d',runname,dnum))
end
fprintf('\nCorrelation collapse plots complete.\n')
% fprintf('Plotrun complete.\n')
end

% compute mean of timeseries data
function result = cmean(cellin)
extradim=1+ndims(cellin{1});
bigarray=cat(extradim,cellin{:});
result = mean(bigarray,extradim,'omitnan');
end

%% single-time errors encountered:
% >> plotrun('2021-09-12_17-19-17')
% Warning: Unexpected end-of-file while reading compressed data.
% > In plotrun (line 48)
% Error using load
% Cannot read file /nfs_physics/users/stud/ripbe022/code/figs/2021-09-12_17-19-17/r2/data/timedata.mat.
%
% Error in plotrun (line 48)
%     St(dnum) = load([datapath 'timedata.mat']);
%
% >> plotrun('2021-09-12_17-19-17')
% Subscripted assignment between dissimilar structures.
%
% Error in plotrun (line 48)
%     St(dnum) = load([datapath 'timedata.mat']);

% had another error to do with legend (invalid argument or something), fixed by restarting tmux session
% Error using legend>process_inputs (line 554)
% Invalid argument. Type 'help legend' for more information.
%
% Error in legend>make_legend (line 306)
% [autoupdate,orient,location,position,children,listen,strings,propargs] = process_inputs(ha,argin); %#ok
%
% Error in legend (line 259)
%     make_legend(ha,args(arg:end),version);
%
% Error in plotrun (line 249)
%     legend('interpreter','latex','box','off','FontSize',18)
%
% 554         error(message('MATLAB:legend:UnknownParameter'));
% K>>