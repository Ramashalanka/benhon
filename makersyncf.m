dateStr = '20210831104501'; kv = 1:17; symsetmax = -1;
%dateStr = '20210830165604'; kv = 1:27; symsetmax = 1;
paritymax = 0;

filelist = {'datavf.mat','psifail.mat','datav-1.mat'};

rsyncf = fopen('rsyncf.txt','w');
for knum = kv
    for fnum = 1:numel(filelist)
        fprintf(rsyncf, '%sr%d/%s\n', dateStr, knum, filelist{fnum});
    end
    for symset = 0:symsetmax
        for parity = 0:paritymax
            fprintf(rsyncf, '%sr%d/dmum%dp%d.mat\n', dateStr, knum, symset, parity);
        end
    end
end

% in ~/Documents/matlabdata/D3Ddb/data
% rsync --stats --progress -av --files-from=../../../matlab/mixbdg/rsyncf.txt -e ssh  thunderbird23.px.otago.ac.nz:Documents/matlabdata/D3Ddb/data/. .
% rsync --stats --progress -av --files-from=../../../matlab/mixbdg/rsyncf.txt -e ssh . mahuika:Documents/matlabdata/D3Ddb/data/.