function plotspinangle(psit,tv,dataall,dtime,inputs,ti)
h1 = figure(1);
clf(h1)

grids = inputs.grids;
data = dataall.data;
plotdata = dataall.plotdata;
minx = gather(min(grids.x));
miny = gather(min(grids.y));

% set plot size/location
set(gcf,'position', [1          41        1920         963])
% Danny:    1036         400        1276         896
% M1:       -1279          43        1280         907
% M2:       1          41        1920         963
clims = [-5 1];

% correlation function
subplot(2,6,[1 2])
avksq = data.EK0/2/data.Nv(2)/inputs.N0; % /2 is to get kx^2 or ky^2 from kx^2+ky^2, EK0 has /2 from KE which is kept for n1r behaviour
rall = linspace(0,1/sqrt(avksq)+1e-10,1e5);
% original data (pre binning)
% plot(inputs.grids.R(:),imag(dataall.n1r(:))/Nv(2)/inputs.N0,'+r','MarkerSize',2)   % correlation (imag)
% hold on
% plot(inputs.grids.R(:),real(dataall.n1r(:))/Nv(2)/inputs.N0,'xb','MarkerSize',2)   % correlation (real)
plot(rall,max(0,1-avksq*rall.^2),'k-','linewidth',1)                                 % comparison to k^2
hold on
% binned data
plot(plotdata.rmean,plotdata.yImmean,'+r','MarkerSize',2)
plot(plotdata.rmean,plotdata.yRemean,'xb','MarkerSize',2)
% circles for width measurement position
Lvalues = [0 inputs.Ly*plotdata.yRemean(1)];
Lstyle = 'kr';
for Ltype = 1:2
    plot(data.Lpos(Ltype),Lvalues(Ltype),[Lstyle(Ltype) 'o']);
end
xlabel('$r$','FontSize',18)
ylabel('')
title('$n^{(1)}_0(r)/N_0$','FontSize',18)
xlim([0 inputs.gi.maxX])
ylim([-0.5 1])
legend('hide')

% time plots
if ti > 0
    
    % L and Lv over time
    subplot(2,6,[3 4])
    hold on
    for Ltype = 2 % 1: 0 crossing, 2: 0.25 crossing
        Lthis = dtime.Lt(Ltype,:);
        Luse = ~isnan(Lthis);
        hL = plot(tv(Luse),Lthis(Luse),[Lstyle(Ltype) '-'],'DisplayName','$L$');
    end
    plot(tv(Luse),dtime.Lvt(Luse),'b','DisplayName','$L_v$')
    uistack(hL,'top')
    xlabel('$t/t_s$','FontSize',18)
    title('$L$','FontSize',18)
    l=legend;
    set(l,'interpreter','latex','box','off','FontSize',12,'Location','southeast')
    xlim([0 tv(ti+1)]) % inputs.t_max])
    
    % N over time
    subplot(2,6,[5 6])
    hold on
    for mnum = 1:3
        avnthis = dtime.allavn(mnum,:);
        avnuse = ~isnan(avnthis);
        plot(tv(avnuse),avnthis(avnuse))
    end
    xlim([0 tv(ti+1)]) % inputs.t_max])
    l=legend('$N_+$','$N_0$','$N_-$');
    set(l,'interpreter','latex','box','off')
    xlabel('$t/t_s$','FontSize',18)
    title('$N_m/N_0$','FontSize',18)
    
    % log vortex number over time
    subplot(2,6,[11 12])
    hold on
    vstyle = {'-','--'};
    for vi = 1:2 % positive and negative vortices
        vthis = dtime.vnumt{1}(vi,:);
        vuse = ~isnan(vthis);
        plot(tv(vuse),log10(vthis(vuse)),vstyle{vi});
    end
    xlim([0 tv(ti+1)]) % inputs.t_max])
    l=legend('$v_+$','$v_-$');
    set(l,'interpreter','latex','box','off')
    xlabel('$t/t_s$','FontSize',18)
    title('$\log_{10}v_{\mathrm{num}}$','FontSize',18)
end

% density plot
subplot(2,6,[7 8])
imagesc(grids.x,grids.y,log10(abs(psit{2}).^2))
title('$\log_{10}|\psi_0|^2$','FontSize',18)
set(gca,'ydir','normal','clim',clims)
xlim(minx*[1 -1])
ylim(miny*[1 -1])
axis(gca,'image','tight') % daspect([1 1 1])
colormap(gca,'parula')
colorbar

% phase plot
subplot(2,6,[9 10])
hold on
phase = angle(psit{2});
imagesc(grids.x,grids.y,phase.') % note we missed the .' until 11/08/21, so all jpg plots generated prior actually need flipping around the x=y axis
title('$\arg\psi_0$','FontSize',18)
set(gca,'ydir','normal','clim',[-pi,pi])
xlim(minx*[1 -1])
ylim(miny*[1 -1])
axis(gca,'image','tight') % daspect([1 1 1])
colormap(gca,'hsv')
colorbar
% plot vortices on phase plot
vpos = plotdata.vpos(1);
scatter(grids.x(vpos.vpx),grids.y(vpos.vpy),50,'k+','linewidth',1)
scatter(grids.x(vpos.vnx),grids.y(vpos.vny),50,'ko','linewidth',1)


% main plot titles
dateStr = strrep(inputs.currdate,'_','\_');
axes('Position',[0 0 1 1],'Visible','off');
text(0.1,0.98,sprintf('$t/t_s=%.2f$, $N=%.3f,(%.3f,%.3f,%.3f)$, $\\log_{10}n_{\\mathrm{err}}=%.1f$, $\\log_{10}E_{\\mathrm{err}}=%.1f$, $\\langle F_z^2\\rangle=%.1f$, $\\langle F_\\perp^2\\rangle=%.1f$, $\\langle\\mathcal{A}^2\\rangle=%.1f$, $v_+=%d$, $v_-=%d$',...
    tv(ti+1), sum(data.Nv), data.Nv, log10(abs(inputs.ninitial-sum(data.Nv))), log10(abs(inputs.Einitial/data.Etotal-1)), data.Fz2,data.Fperp2,data.align2,data.vnum(1,:)),'FontSize',18)
text(0.1,0.02,sprintf('%sr%d: $n_0=%.1f$, $(X,Y)=(%0.f,%0.f)$, $(N_x,N_y)=(%0d,%0d)$, $q=%.2f$, $c_0=%.2f$, $c_1=%.2f$, $t_\\mathrm{step}=%.2g$, $\\Delta t=%.2g$, $\\sigma=%.2f$', ...
    dateStr, inputs.knum, inputs.n0, grids.maxX, grids.maxY, grids.N_x, grids.N_y, inputs.q, inputs.c_0, inputs.c_1, inputs.step_size, inputs.dt, inputs.sigma_v(1)),'FontSize',18)

print('-djpeg',sprintf('%s/pspin/pspin%04d', inputs.runpath, ti/inputs.plotstep)) % dsvg/dpdf for vector
