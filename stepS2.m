% Integrates the spin-1 GPE forward in time using a symplectic algorithm to
% compose the solution of two exactly solvable subsystems.
% Subsystem A: TF/SMA with no quadratic Zeeman
% Subsystem B: kinetic energy + quadratic Zeeman
% (note backwards to A and B in Symes2016)
%
% Inputs:
% psi0      - cell with three initial wavefunction components mF=+1,0,-1
% dt        - timestep to find wavefunction solution at
% p         - linear Zeeman strength (can be scalar or position dependent)
% q         - quadratic Zeeman strength (must be scalar for this algorithm)
% c0        - spin-independent interaction strength
% c1        - spin-dependent interaction strength
% V0        - trapping potential (can be scalar or position dependent)
% Outputs:
% psi_t     - wavefunction at final time (same size as psi0)

% function psi_t = stepS2(psivec, step_size, dt, p, q, c1, c0, V0, A, B)
function psi_t = stepS2(psivec, inputs)
if nargin < 8
    V0 = 0;
end
step_size = inputs.step_size;
dt = inputs.dt;
sEk = inputs.grids.sEk;
q = inputs.q;

H_A = @(psi_in, t) calc_spin_psi_t(psi_in, inputs.c_0, inputs.c_1, V0, 0, t);
 
H_B = @(psi_in, t) {
    ifftn(exp(-1i*(sEk + q)*t).*fftn(psi_in{1}));
    ifftn(exp(-1i*sEk*t).*fftn(psi_in{2}));
    ifftn(exp(-1i*(sEk + q)*t).*fftn(psi_in{3}))
    };
 
% Join multiple steps together (~1/2 the number of H_A op's).
numSteps = floor(step_size/dt);
psi_t = H_A(psivec, dt/2);

for ii=1:(numSteps - 1)
    psi_t = H_A(H_B(psi_t, dt), dt);
end
psi_t = H_A(H_B(psi_t, dt), dt/2);
  
 
% finish remainder time:
dtr = step_size-numSteps*dt;
 
if(dtr>1e-12) % remainder step
psi_t = H_A(H_B(H_A(psi_t, dtr/2), dtr), dtr/2);
end 
 
 
 
 
 
 
 
function psi_t = spin1_psi_t(psi0, c0, c1, V0, p, t)
if isa(psi0{1}, 'gpuArray')
    [psi_t{1}, psi_t{2}, psi_t{3}] = arrayfun(@calc_spin_psi_t_gpu, psi0{1}, psi0{2}, psi0{3}, c0, c1, V0, p, t);
else
    [psi_t{1}, psi_t{2}, psi_t{3}] = calc_spin_psi_t(psi0{1}, psi0{2}, psi0{3}, c0, c1, V0, p, t);
end
 
 
function [psiP_t, psi0_t, psiM_t] = calc_psi(psiP, psi0, psiM, c0, c1, V0, p, t)
n = abs(psiP).^2 + abs(psi0).^2 + abs(psiM).^2;
 
alpha = psi0.^2 - 2*psiP.*psiM;
F = sqrt(complex(n.^2 - abs(alpha).^2));
A = cos(c1*F*t);
B = sin(c1*F*t)./F;
B(F==0) = 0;
if p==0
    C = exp(-1i*(V0 + c0*n)*t);
    psiP_t = ( psiP.*A - 1i*(n.*psiP + alpha.*conj(psiM)).*B ).*C;
    psi0_t = ( psi0.*A - 1i*(n.*psi0 - alpha.*conj(psi0)).*B ).*C;
    psiM_t = ( psiM.*A - 1i*(n.*psiM + alpha.*conj(psiP)).*B ).*C;
else
    psiP_t = ( psiP.*A - 1i*(n.*psiP + alpha.*conj(psiM)).*B ).*exp(-1i*(V0 + c0*n - p)*t);
    psi0_t = ( psi0.*A - 1i*(n.*psi0 - alpha.*conj(psi0)).*B ).*exp(-1i*(V0 + c0*n)*t);
    psiM_t = ( psiM.*A - 1i*(n.*psiM + alpha.*conj(psiP)).*B ).*exp(-1i*(V0 + c0*n + p)*t);
end 
 
 
function [n, alpha, F] = calc_terms(psiP, psi0, psiM)
n = abs(psiP).^2 + abs(psi0).^2 + abs(psiM).^2;
alpha = psi0.^2 - psiP.*psiM;
F2 = n^2 - abs(alpha)^2;
if F2 < 0
    F = 0;
else
    F = sqrt(n.^2 - abs(alpha).^2);
end
 
 
function A = calc_A(F, omega)
A = cos(omega*F);
 
 
function B = calc_B(F, omega)
if F == 0
    B = 0*omega;
else
    B = sin(omega*F)./F;
end
 
 
function psiPM = calc_psiPM(psiPM, psiMP, n, alpha, A, B, C)
psiPM = ( psiPM.*A - 1i*(n.*psiPM + alpha.*conj(psiMP)).*B ).*C;
 
 
function psi0 = calc_psi0(psi0, n, alpha, A, B, C)
psi0 = ( psi0.*A - 1i*(n.*psi0 - alpha.*conj(psi0)).*B ).*C;
 
 
 
 
 
function psi_t = psi_kinetic_q(psi0, t, Ek, q, projk)
 
if nargin < 5
    A = exp(-1i*(Ek + q)*t);
    B = exp(-1i*Ek*t);
%     psi = fft2(cat(3, psi0{:}));
%     psi(:,:,1) = A.*psi(:,:,1);
%     psi(:,:,2) = B.*psi(:,:,2);
%     psi(:,:,3) = A.*psi(:,:,3);
%     psi = ifft2(psi);
%     psi_t = {psi(:,:,1); psi(:,:,2); psi(:,:,3)};
    psi_t = {
    ifftn(A.*fftn(psi0{1}));
    ifftn(B.*fftn(psi0{2}));
    ifftn(A.*fftn(psi0{3}))
    };
else
    psi_t = {
    ifftn(projk.*exp(-1i*(Ek + q)*t).*fftn(psi0{1}));
    ifftn(projk.*exp(-1i*Ek*t).*fftn(psi0{2}));
    ifftn(projk.*exp(-1i*(Ek + q)*t).*fftn(psi0{3}))
    };
end
 
% fz0 = abs(psi0{1}).^2 - abs(psi0{3}).^2;
% fz1 = abs(psi_t{1}).^2 - abs(psi_t{3}).^2;
% sum(fz0(:)) - sum(fz1(:))
 
 
 
function [psiout] = calc_spin_psi_t(psiin, c0, c1, V0, p, t)
psiP=psiin{1};
psi0=psiin{2};
psiM=psiin{3};
n = abs(psiP).^2 + abs(psi0).^2 + abs(psiM).^2;
 
alpha = psi0.^2 - 2*psiP.*psiM;
F = sqrt(complex(n.^2 - abs(alpha).^2));
A = cos(c1*F*t);
B = sin(c1*F*t)./F;
B(F==0) = 0;
if p==0
    C = exp(-1i*(V0 + c0*n)*t);
%     psiP_t = ( psiP.*A - 1i*(n.*psiP + alpha.*conj(psiM)).*B ).*C;
%     psi0_t = ( psi0.*A - 1i*(n.*psi0 - alpha.*conj(psi0)).*B ).*C;
%     psiM_t = ( psiM.*A - 1i*(n.*psiM + alpha.*conj(psiP)).*B ).*C;
    psiout{1} = ( psiP.*A - 1i*(n.*psiP + alpha.*conj(psiM)).*B ).*C;
    psiout{2} = ( psi0.*A - 1i*(n.*psi0 - alpha.*conj(psi0)).*B ).*C;
    psiout{3} = ( psiM.*A - 1i*(n.*psiM + alpha.*conj(psiP)).*B ).*C; 
else
  %  psiP_t = ( psiP.*A - 1i*(n.*psiP + alpha.*conj(psiM)).*B ).*exp(-1i*(V0 + c0*n - p)*t);
  %  psi0_t = ( psi0.*A - 1i*(n.*psi0 - alpha.*conj(psi0)).*B ).*exp(-1i*(V0 + c0*n)*t);
  %  psiM_t = ( psiM.*A - 1i*(n.*psiM + alpha.*conj(psiP)).*B ).*exp(-1i*(V0 + c0*n + p)*t);
 
    psiout{1} = ( psiP.*A - 1i*(n.*psiP + alpha.*conj(psiM)).*B ).*exp(-1i*(V0 + c0*n - p)*t);
    psiout{2} = ( psi0.*A - 1i*(n.*psi0 - alpha.*conj(psi0)).*B ).*exp(-1i*(V0 + c0*n)*t);
    psiout{3} = ( psiM.*A - 1i*(n.*psiM + alpha.*conj(psiP)).*B ).*exp(-1i*(V0 + c0*n + p)*t);
end
 
 
% Solves the kinetic + quadratic Zeeman spin-1 subsystem
%
% Inputs:
% psi0      - cell with three initial wavefunction components mF=+1,0,-1
% t         - final time to find wavefunction solution at
% Ek        - kinetic energy term (fftshifted)
% q         - quadratic Zeeman strength (must be scalar for this algorithm)
% Outputs:
% psi_t     - wavefunction at final time (same size as psi0)
 
function psi_t = psi_kinetic_q3(psi0, A, B)
psi_t = {
ifftn(A.*fftn(psi0{1}));
ifftn(B.*fftn(psi0{2}));
ifftn(A.*fftn(psi0{3}))
};
 

